# Transcript of Pepper&Carrot Episode 28 [fr]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titre|1|False|Épisode 28 : Les festivités

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrateur|1|True|Les trois lunes d'Hereva étaient alignées cette nuit-là,
Narrateur|2|False|et leur éclat offrait un fabuleux spectacle dans la cathédrale de Zombiah.
Narrateur|3|False|Ce fut sous cette lumière magique que Coriandre, mon amie, devint...
Narrateur|4|False|Reine de Qualicity.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrateur|1|False|Les festivités débutèrent peu après.
Narrateur|2|False|Une immense fête avec toutes les écoles de magie, et des centaines d'invités prestigieux venus de tout Hereva.
Pepper|3|False|?!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Oh, c'est toi !
Pepper|3|False|J'étais perdue dans mes pensées.
Carrot|2|False|frou frou
Pepper|4|False|Je vais à l'intérieur. Il commence à faire froid dehors.
Journaliste|6|False|Vite !
Journaliste|5|False|La voilà !
Journaliste|7|False|Mademoiselle Safran ! Quelques mots pour le Qualicity Daily ?
Safran|8|False|Oui, bien sûr !
Pepper|15|False|...
Pepper|16|False|Tu vois, Carrot ? Je pense que je comprends pourquoi je ne suis pas dans l'ambiance.
Son|9|False|F LASH|nowhitespace
Journaliste|13|True|Mademoiselle Safran ! Gazette Hereva Style.
Son|10|False|F LASH|nowhitespace
Son|11|False|F LASH|nowhitespace
Son|12|False|F LASH|nowhitespace
Journaliste|14|False|Quel couturier portez-vous ce soir ?

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Toutes mes amies ont du succès, et parfois j'aimerais avoir le quart de ce qu'elles ont.
Pepper|2|False|Coriandre est une reine.
Pepper|3|False|Safran est riche et célèbre.
Pepper|4|False|Même Shichimi semble parfaite avec son école.
Pepper|5|False|Et moi ? Qu'est-ce que j'ai ?
Pepper|6|False|Ça.

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|False|Pepper ?
Shichimi|2|False|J'ai pris à manger, tu en veux ?

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|True|C'est bizarre d'avoir à se cacher pour manger.
Shichimi|2|False|Mais notre maître dit qu'on doit avoir l'air d'esprits purs sans besoins matériels.
Safran|3|False|Ah ! Vous voilà !
Safran|4|True|Enfin ! Un endroit pour me cacher des photographes !
Safran|5|False|Ils me rendent folle !
Coriandre|6|False|Des photographes ?
Coriandre|7|False|Essayez plutôt d'éviter les discussions ennuyeuses avec les politiciens quand vous avez ça sur la tête.
Son|8|False|grat' grat'

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Coriandre|1|True|Au fait, Pepper, j'ai enfin rencontré tes marraines.
Coriandre|2|False|Elles sont vraiment « spéciales ».
Pepper|3|False|!!!
Coriandre|4|True|Je veux dire, tu as beaucoup de chance.
Coriandre|5|False|Je suis sûre qu'elles te laissent faire ce que tu veux.
Coriandre|6|False|Comme laisser tomber les poses officielles et bavardages inutiles sans risquer un incident diplomatique.
Safran|7|False|Ou danser et s'amuser sans se soucier du regard des autres.
Shichimi|8|False|Ou pouvoir goûter les plats du buffet devant tout le monde !

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrateur|4|False|À SUIVRE…
Shichimi|1|False|Oh ! Pepper ?! A-t-on dit quelque chose de mal ?
Pepper|2|True|Non, pas du tout !
Pepper|3|False|Merci, les amies !

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|5|True|Vous pouvez aussi devenir mécène de Pepper&Carrot et avoir votre nom inscrit ici !
Pepper|3|True|Pepper&Carrot est entièrement libre, gratuit, open-source et sponsorisé grâce au mécénat de ses lecteurs.
Pepper|4|False|Cet épisode a reçu le soutien de 960 mécènes !
Pepper|7|True|Allez sur www.peppercarrot.com pour plus d'informations !
Pepper|6|True|Nous sommes sur Patreon, Tipeee, PayPal, Liberapay ...et d'autres !
Pepper|8|False|Merci !
Pepper|2|True|Le saviez-vous ?
Crédits|1|False|Janvier 2019 Art & scénario : David Revoy. Lecteurs de la version bêta : CalimeroTeknik, Craig Maloney, Martin Disch, Midgard, Nicolas Artance, Valvin. Version française Traduction : CalimeroTeknik, Nicolas Artance, Valvin. Basé sur l'univers d'Hereva Créateur : David Revoy. Mainteneur principal : Craig Maloney. Écrivains : Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Correcteurs : Willem Sonke, Moini, Hali, CGand, Alex Gryson . Logiciels : Krita 4.1.5~appimage, Inkscape 0.92.3 on Kubuntu 18.04.1. Licence : Creative Commons Attribution 4.0. www.peppercarrot.com
<hidden>|0|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|0|False|You can also translate this page if you want.
<hidden>|0|False|Beta readers help with the story, proofreaders give feedback about the text.
