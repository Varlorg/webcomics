#!/bin/bash
#
#  generate-all-transcripts.sh
#
#  SPDX-License-Identifier: GPL-3.0-or-later
#
#  Copyright 2019-2020 GunChleoc <fios@foramnagaidhlig.net>
#
#  Script for creating CSV transcripts and their HTML output for all
#  episodes and locales
#

scriptdir="0_transcripts"

echo "This script generates transcripts for all episodes."
echo "If no locale is specified, generates them for all languages"
echo " "
echo "Usage:   bash 0_transcripts/generate-all-transcripts.sh [<locale>]"
echo "Example: bash 0_transcripts/generate-all-transcripts.sh fr"
echo " "

# Check if we have a 2-letter locale code specified on the command line
language="*"
if [[ $# -eq 1 ]] ; then
  language=$1
fi

if [[ ${#language} -ne 2 ]] ; then
    language="*"
    echo "Generating transcripts for all locales"
else
    echo "Generating transcripts for locale: ${language}"
fi
echo " "

# Move up if we're not in the base directory.
if [ -d "../$scriptdir" ]; then
    pushd ..
fi

declare -i md_errors; md_errors=0
declare -i html_errors; html_errors=0

# Find all episodes/locales and generate transcripts
for episodedir in *; do
    if [ -d "${episodedir}" ] ; then
        # We have a directory. Search for any locales
        for localedir in ${episodedir}/lang/${language}; do
            if [ -d "${localedir}" ] ; then
                # Only process directories that have svg files in them
                for svgfile in ${localedir}/*.svg; do
                    if [ -f "$svgfile" ] ; then
                        # There is an svg file for this episode and locale.
                        # Generate transcripts it and then break to the next locale.

                        # Get the locale from the SVG file's path
                        locale=$(basename $(dirname $svgfile))

                        # Regenerate transcript Markdown and HTML output.
                        # Script output is noisy, so redirecting it to nowhere.
                        echo "Generating HTML transcript for $episodedir $locale"
                        $scriptdir/extract_text.py $episodedir $locale > /dev/null
                        if [[ ! $? -eq 0 ]] ; then
                            echo "ERROR: Updating transcript encountered an error for $episodedir $locale"
                            md_errors+=1
                        fi
                        $scriptdir/extract_to_html.py $episodedir $locale > /dev/null
                        if [[ ! $? -eq 0 ]] ; then
                            echo "WARNING: Generating HTML encountered an error for $episodedir $locale"
                            html_errors+=1
                        fi
                        break
                    fi
                done
            fi
        done
    fi
done

if [[ (${md_errors} > 0 )]] ; then
    echo "###################################"
    echo "  Found ${md_errors} Markdown error(s)"
    echo "###################################"
fi

if [[ (${html_errors} > 0 )]] ; then
    echo "###################################"
    echo "  Found ${html_errors} HTML warning(s)"
    echo "###################################"
else
    echo "###################################"
    echo "  All transcripts are OK :)"
    echo "###################################"
fi

if [[ (${md_errors} > 0 ) ]] ; then
    exit 1
fi
