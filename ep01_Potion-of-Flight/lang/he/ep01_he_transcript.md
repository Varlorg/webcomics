# Transcript of Pepper&Carrot Episode 01 [he]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
כותרת|1|False|פרק ראשון: שיקוי התעופה

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
פפר|1|False|ואחרון חביב...
פפר|4|False|מממ... זה בטח לא מספיק חזק
צליל|2|True|פשש
צליל|3|False|פשש
צליל|5|True|פלופ
צליל|6|False|פלופ

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
פפר|1|False|...אה מושלם
פפר|2|False|!לא אל תחשוב על זה אפילו
צליל|3|False|ספלאש|nowhitespace

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
פפר|1|False|?! שמח
קרדיט|2|False|WWW.PEPPERCARROT.COM 05/2014
