# Transcript of Pepper&Carrot Episode 01 [fr]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titre|1|False|Épisode 1 : Potion d'envol

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|...et la dernière touche
Pepper|4|False|...mmm probablement pas assez
Son|2|True|SHH
Son|3|False|SHH
Son|5|True|PLOP
Son|6|False|PLOP

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|ha... parfait
Pepper|2|False|NON! N'y pense même pas
Son|3|False|S PL A S H|nowhitespace

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Satisfait ?
Crédits|2|False|WWW.PEPPERCARROT.COM 05/2014
