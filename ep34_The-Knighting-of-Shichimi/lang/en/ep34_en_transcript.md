# Transcript of Pepper&Carrot Episode 34 [en]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Episode 34: The Knighting of Shichimi

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrator|1|False|That same night...
Hibiscus|2|False|...and so, tonight, we welcome you, Shichimi, as our youngest Knight of Ah.
Coriander|3|False|Still no trace of Pepper?
Saffron|4|False|Not yet.
Saffron|5|False|She'd better hurry, or she'll miss Shichimi's speech.
Shichimi|6|False|Thank you.
Shichimi|7|False|I'd like to...

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|False|Zio oOOOOO|nowhitespace
Sound|2|False|Zioo O O O OO|nowhitespace
Pepper|3|True|Incoming!
Pepper|4|True|Watch out!
Pepper|5|False|WATCH OUT!!!
Sound|6|False|CR A S H!|nowhitespace
Sound|7|False|Whoops!
Sound|8|False|Is everyone OK? Nothing broken?
Shichimi|9|False|Pepper!
Pepper|10|True|Hello Shichimi!
Pepper|11|True|Sorry for the dramatic entrance and for being late!
Pepper|12|False|I've been running all day, but that's a long story.
<hidden>|13|False|Dear translator: if editing the soundFX is difficult, I made a special documentation here: https://www.peppercarrot.com/en/static14/documentation&page=055_Sound-Effect_translation

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Hibiscus|1|False|Is she one of your guests?
Shichimi|2|False|Yes. She is my friend Pepper. Everything is fine.
Pepper|3|True|Carrot, are you OK?
Pepper|4|False|Sorry about the landing, I haven’t mastered it yet while using hyperspeed.
Pepper|5|True|And again sorry to everyone for the trouble,
Pepper|6|False|and as for how I’m dressed...
Shichimi|7|False|tee hee
Wasabi|8|True|Shichimi,
Wasabi|9|False|this young witch who has arrived, is she really a friend of yours?

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|True|Yes, your highness.
Shichimi|2|False|Her name is Pepper, of the School of Chaosah.
Wasabi|3|True|Her presence here pollutes the sacred nature of our school.
Wasabi|4|False|Get her out of my sight, immediately.
Shichimi|5|True|But...
Shichimi|6|False|Master Wasabi...
Wasabi|7|True|But what ?
Wasabi|8|False|Would you prefer to be banished from our school?
Shichimi|9|False|! ! !|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|True|Sorry Pepper, but you need to leave.
Shichimi|2|False|Now.
Pepper|3|True|Huh?
Pepper|4|False|Hey hey hey, wait a minute! I'm sure there's a misunderstanding.
Shichimi|5|False|Please Pepper, don't make this difficult.
Pepper|6|False|Hey! You there, on the throne. If you have a problem with me, come down and tell me yourself!
Wasabi|7|False|Tsss...
Wasabi|8|True|Shichimi, you have ten seconds...
Wasabi|9|True|nine...
Wasabi|10|True|eight...
Wasabi|11|False|seven...
Shichimi|12|False|ENOUGH, PEPPER! GO AWAY!!!
Sound|13|False|SHRRIiii!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Shichimi, please calm d...
Sound|2|False|B ADuuM!|nowhitespace
Shichimi|3|True|GO AWAY!!!
Shichimi|4|True|GO AWAY!!!
Shichimi|5|False|GO AWAY!!!
Sound|6|False|C R E E E E E! !!|nowhitespace
Pepper|7|True|Ouch!
Pepper|8|False|Hey! That's... n-NOT... Owww... Nice!
Coriander|9|False|SHICHIMI! PEPPER! PLEASE STOP!
Saffron|10|False|Wait.
Wasabi|11|False|Hmm!
Pepper|12|True|Grrr!
Pepper|13|False|OK, you asked for it!
Sound|14|False|B R Z OO!!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|CURSUS CANCELLARE MAXIMUS!!!
Sound|2|False|S H K L AK!|nowhitespace
Pepper|3|False|Ouch!
Sound|4|False|P AF !!|nowhitespace
Shichimi|5|True|Even your best cancellation spell has no effect on me!
Shichimi|6|True|Give up, Pepper, and go away!
Shichimi|7|False|Stop making me hurt you!
Pepper|8|False|Oh, my cancellation spell worked just fine, but you weren't the target.
Shichimi|9|True|Huh?
Shichimi|10|False|What do you mean?!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|False|?!!
Pepper|2|True|She was the target!
Pepper|3|False|I just canceled her Glamour Spell which keeps her looking young.
Pepper|4|True|I noticed this spell as soon as I got here.
Pepper|5|False|So, I gave you a small token of what you deserve for making Shichimi fight me!
Wasabi|6|True|INSOLENCE!
Wasabi|7|True|How dare you,
Wasabi|8|False|and in front of my whole school!
Pepper|9|True|Consider yourself lucky!
Pepper|10|False|If I had all of my Rea, I wouldn't have stopped there.

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|False|DR Z OW!!|nowhitespace
Wasabi|2|True|I see. You have reached the level of your predecessors much sooner than I anticipated...
Wasabi|3|False|This accelerates my plans, but that is good news.
Pepper|4|True|Your plans?
Pepper|5|True|So you were just testing me, and this had nothing to do with my being late?
Pepper|6|False|You really are twisted!
Wasabi|7|True|tee...
Wasabi|8|False|hee.
Wasabi|9|True|WHAT ARE YOU ALL STARING AT?!
Wasabi|10|False|I WAS JUST ATTACKED, AND YOU DO NOTHING BUT STAND THERE?! GET HER!!!
Wasabi|11|False|I WANT HER ALIVE!
Wasabi|12|False|GET HER!!!
Pepper|13|False|Shichimi, we'll have to talk about this later!
Pepper|14|True|I’m sorry, Carrot, but we'll have to take off at hyperspeed again.
Pepper|15|False|Hold on tight!
Sound|16|False|Tap!
Sound|17|False|Tap!
Sound|18|False|Ziioo OO!!

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|? !
Wasabi|2|False|GET HER!!!
Pepper|3|False|Oh dear.
Saffron|4|False|Pepper, take my broom!
Sound|5|False|Fizzz!
Pepper|6|True|Oh wow!
Pepper|7|False|Thank you, Saffron!
Sound|8|False|Toc!
Sound|9|False|zioo O O O O!|nowhitespace
Narrator|10|False|TO BE CONTINUED...

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|March 31, 2021 Art & scenario: David Revoy. Beta readers: Arlo James Barnes, Carotte, Craig Maloney, Efrat b, GunChleoc, Karl Ove Hufthammer, Martin Disch, Nicolas Artance, Parnikkapore, Valvin. English version (original version) Proofreading: Arlo James Barnes, Carotte, Craig Maloney, GunChleoc, Karl Ove Hufthammer, Martin Disch. Special thanks: to Nartance for exploring the Wasabi character in his fan-fiction novels. The way he imagined her had a big impact on the way I depicted her in this episode. Based on the universe of Hereva Creator: David Revoy. Lead maintainer: Craig Maloney. Writers: Craig Maloney, Nartance, Scribblemaniac, Valvin. Correctors: Willem Sonke, Moini, Hali, CGand, Alex Gryson . Software: Krita 4.4.1, Inkscape 1.0.2 on Kubuntu Linux 20.04. License: Creative Commons Attribution 4.0. www.peppercarrot.com
Pepper|2|True|Did you know?
Pepper|3|True|Pepper&Carrot is entirely free/libre, open-source and sponsored thanks to the patronage of its readers.
Pepper|4|False|For this episode, thanks go to 1096 patrons!
Pepper|5|True|You too can become a patron of Pepper&Carrot and get your name here!
Pepper|6|True|We are on Patreon, Tipeee, PayPal, Liberapay ...and more!
Pepper|7|False|Check www.peppercarrot.com for more info!
Pepper|8|False|Thank you!
<hidden>|9|False|You can also translate this page if you want.
<hidden>|10|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|11|False|Beta readers help with the story, proofreaders give feedback about the text.
