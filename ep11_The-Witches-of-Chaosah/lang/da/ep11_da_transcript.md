# Transcript of Pepper&Carrot Episode 11 [da]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 11: Kaosah-heksene

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|1|False|Pepper, du kaster skam over Kaosah
Timian|2|False|Cayenne har ret; en ægte Kaosah-heks skal frembringe frygt, lydighed og respekt
Spidskommen|3|False|... mens du derimod tilbyder cupcakes og the selv til vores dæmoner*...
Note|4|False|* Se episode 8: Peppers fødselsdag
Pepper|5|True|Men...
Pepper|6|False|...gudmødre...

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|4|False|TI STILLE!
Spidskommen|9|False|Dadaaa!
Timian|1|True|Pepper, du er uden tvivl talentfuld, men du er også vores eneste efterfølger.
Timian|2|False|Det er vores pligt at gøre dig til en ægte ond Kaosah-heks
Pepper|3|False|Men... jeg vil ikke være ond! Det... det er mod min...
Cayenne|5|True|... ellers tilbagekalder vi alle dine kræfter!
Cayenne|6|False|Og du bliver igen den lille dumme forældreløse pige ved Egerns Ende
Timian|7|False|Spidskommen vil fra nu af følge dig overalt for at oplære dig og sikre, at du gør fremskridt
Lyd|8|False|Puuf!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Spidskommen|1|False|Den nye Konge af Akren er for god og sød mod sine undersåtter. Folket skal frygte deres Konge.
Spidskommen|2|False|Din første opgave er at skræmme denne monark for bedre at manipulere ham
Spidskommen|3|False|En ægte Kaosah-heks har indflydelse på de magtfulde!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Kongen?...
Pepper|2|False|... så ung?!
Pepper|3|False|Vi er nok lige gamle...
Spidskommen|4|False|Men hvad venter du på! Kom så! Væk ham, belær ham, gør ham bange!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|1|False|Dig og mig... vi er meget ens...
Pepper|2|False|unge...
Pepper|3|True|alene...
Pepper|4|True|... fanger af vores skæbne
Pepper|5|False|Dzzz

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Timian|6|False|Vi har meget arbejde at gøre med dig, Pepper...
Credits|8|False|09/2015 – Tegning og manuskript: David Revoy – Oversættelse: Emmiline, Rikke & Alexandre Alapetite
Fortæller|7|False|- Slut -
Lyd|1|False|?!
Pepper|2|False|Første test:
Lyd|3|False|DUMPET!
Timian|4|True|Puuf!
Timian|5|True|Fluf!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Pepper&Carrot er helt gratis, open-source, og sponsoreret af læsere. Denne episode kunne ikke være blevet til uden støtte fra de 502 tilhængere:
Credits|3|False|https://www.patreon.com/davidrevoy
Credits|2|True|Støt næste episode af Pepper&Carrot; hver en krone gør en forskel
Credits|4|False|Licens: Creative Commons Kreditering 4.0 Kildematerialet: tilgængelige på www.peppercarrot.com Værktøj: denne episode er 100% designet med fri software: Krita 2.9.6, G'MIC 1.6.5.2, Inkscape 0.91 på Linux Mint 17
