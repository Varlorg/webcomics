# Transcript of Pepper&Carrot Episode 32 [da]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 32: Slagmarken

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Kongen|1|True|Officer!
Kongen|2|False|Fandt du en heks, som jeg beordrede?
Officer|3|True|Ja, min herre!
Officer|4|False|Hun står lige ved siden af Dem.
Kongen|5|False|...?
Pepper|6|True|Hej!
Pepper|7|False|Mit navn er Pepp...
Kongen|8|False|?!!
Kongen|9|True|FJOLS!!!
Kongen|10|True|Hvorfor kommer du med et barn?!
Kongen|11|False|Jeg skal bruge en rigtig heks!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Undskyld mig!
Pepper|2|True|Jeg er en ægte Kaosah-heks.
Pepper|3|False|Jeg har endda et diplom som...
Skrift|4|True|Kaosah
Skrift|5|True|Diplom
Skrift|6|False|Cayenne
Skrift|8|True|Spidskommen
Skrift|9|False|Timian
Skrift|7|True|~ til Pepper -|nowhitespace
Skrift|10|False|STILLE!
Kongen|11|False|Jeg har ikke brug for et barn i mit hær.
Kongen|12|True|Gå hjem og leg med dine dukker.
Kongen|13|False|Klask!
Hær|14|True|HAHA HA HA!
Hær|15|True|HAHA HA HA!
Hær|16|True|HAHA HA HA!
Hær|17|False|HAHA HAHA!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Jeg kan ikke tro det!
Pepper|2|False|Jeg har studeret i årevis, men ingen vil tage mig seriøst...
Pepper|3|False|... fordi jeg ikke ser erfaren nok ud!
Lyd|4|False|PUF ! !|nowhitespace
Pepper|5|False|CARROT !|nowhitespace
Lyd|6|False|BUM! !|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Seriøst, Carrot?
Pepper|2|True|Jeg håber, maden var det værd.
Pepper|3|False|Du ser hæslig ud...
Pepper|4|False|...Du ser...
Pepper|5|True|Udseendet!
Pepper|6|False|Selvfølgelig!
Lyd|7|False|Knæk!|nowhitespace

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|HEY!
Pepper|2|False|Jeg hørte, at I leder efter en RIGTIG HEKS ?!?

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Kongen|1|True|Efter nærmere overvejelse... tag barnet tilbage.
Kongen|2|False|Det bliver nok for dyrt med hende her.
Skrift|3|False|FORTSÆTTES…

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|5|True|Du kan også blive tilhænger og få dit navn skrevet her!
Pepper|3|True|Pepper & Carrot er fri, open-source og sponsoreret af sine læsere.
Pepper|4|False|Denne episode blev støttet af 1121 tilhængere!
Pepper|7|True|Gå på www.peppercarrot.com og få mere information!
Pepper|6|True|Vi er på Patreon, Tipeee, PayPal, Liberapay … og andre!
Pepper|8|False|Tak!
Pepper|2|True|Vidste I det?
Credits|1|False|31 Marts, 2020 Tegning og manuskript: David Revoy. Genlæsning i beta-versionen: Craig Maloney, Martin Disch, Arlo James Barnes, Nicolas Artance, Parnikkapore, Stephen Paul Weber, Valvin, Vejvej. Dansk Version Oversættelse: Emmiline Alapetite Rettelser: Rikke & Alexandre Alapetite Baseret på Hereva-universet Skabt af: David Revoy. Vedligeholdt af: Craig Maloney. Medforfattere: Craig Maloney, Nartance, Scribblemaniac, Valvin. Rettelser: Willem Sonke, Moini, Hali, CGand, Alex Gryson . Værktøj: Krita 4.2.9-beta, Inkscape 0.92.3 on Kubuntu 19.10. Licens: Creative Commons Attribution 4.0. www.peppercarrot.com
<hidden>|9|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|10|False|You can also translate this page if you want.
<hidden>|11|False|Beta readers help with the story, proofreaders give feedback about the text.
