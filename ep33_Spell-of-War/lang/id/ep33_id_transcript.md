# Transcript of Pepper&Carrot Episode 33 [id]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Judul|1|False|Episode 33: Mantra Perang

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Musuh|1|False|huUWOOOOOOOOOOO !
Tentara|2|False|Rawwwr!
Tentara|5|False|Grrawwr!
Tentara|4|False|Grrrr!
Tentara|3|False|Yuurrr!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Raja|1|True|OK, penyihir muda.
Raja|2|False|Kalau kamu punya mantra yang bisa membantu kita, sekaranglah saatnya!
Pepper|3|True|Baiklah!
Pepper|4|False|Bersiaplah untuk menyaksikan...
Suara|5|False|Dzziii ! !|nowhitespace
Pepper|6|False|...KARYA BESARKU!
Suara|7|False|Dzziiii ! !|nowhitespace
Pepper|8|False|Realitas Hackeris Pepperus!
Suara|9|False|Dzziooo ! !|nowhitespace

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Suara|1|False|Fiizz!
Suara|2|False|Dzii!
Suara|3|False|Schii!
Suara|4|False|Ffhii!
Suara|8|False|Dziing!
Suara|7|False|Fiizz!
Suara|6|False|Schii!
Suara|5|False|Ffhii!
Raja|9|True|Jadi, mantra ini menjadikan pedang kita lebih kuat...
Raja|10|False|...dan memperlemah senjata lawan?
Suara|11|False|Dzii...
Pepper|12|True|Heh.
Pepper|13|True|Kau akan lihat!
Pepper|14|False|Yang aku bisa katakan adalah kamu tidak akan kehilangan satu tentarapun hari ini!
Pepper|15|False|Tapi kalian tetap perlu berperang dan berjuang sebaik-baiknya!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Raja|1|False|Memang untuk itulah kami dilatih.
Raja|2|False|SIAAAAAAAAAAAP!
Tentara|3|False|Yahhhh!
Tentara|4|False|Yeaahh!
Tentara|5|False|Yaaah!
Tentara|6|False|Yaawww!
Tentara|7|False|Yeaaah!
Tentara|8|False|Yaaah!
Tentara|9|False|Yaeeh!
Tentara|10|False|Yaeeh!
Tentara|11|False|Yaahhh!
Raja|12|False|MAJUUUUUUUUUU!!!
Tentara|13|False|Yeaahh!
Tentara|14|False|Yahhh!
Tentara|15|False|Yaeeh!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Raja|1|False|Yahhhh!!!
Musuh|2|False|YUuurr!!!
Suara|3|False|Schwiiing ! !|nowhitespace
Suara|4|False|Swoosh ! !|nowhitespace
Penulis|5|False|12
Musuh|6|False|?!!
Penulis|7|False|8
Raja|8|False|?!!
Penulis|9|False|64
Penulis|10|False|32
Penulis|11|False|72
Penulis|12|False|0
Penulis|13|False|64
Penulis|14|False|0
Penulis|15|False|56
Tentara|20|False|Yrrr!
Tentara|17|False|Yurr!
Tentara|19|False|Grrr!
Tentara|21|False|Yaaah!
Tentara|18|False|Yeaaah!
Tentara|16|False|Yaawww!
Suara|27|False|Sshing
Suara|25|False|Fffchh
Suara|22|False|wiizz
Suara|23|False|Swoosh
Suara|24|False|Chklong
Suara|26|False|Chkilng

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Raja|1|False|?!
Penulis|2|False|3
Penulis|3|False|24
Penulis|4|False|38
Penulis|5|False|6
Penulis|6|False|12
Penulis|7|False|0
Penulis|8|False|5
Penulis|9|False|0
Penulis|10|False|37
Penulis|11|False|21
Penulis|12|False|62
Penulis|13|False|27
Penulis|14|False|4
Raja|15|False|! !|nowhitespace
Raja|16|False|PENYIHIIIR!!!! APA YANG TELAH KAU LAKUKAN?!!!!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Tidak ada lagi kematian!
Pepper|2|True|Tadaaa !
Pepper|3|False|“Ini"
Pepper|4|False|...adalah karya besarku!
Penulis|5|False|Ini adalah mantra kompleks yang mengubah kenyataan dan menunjukkan sisa nyawa dari lawan.
Pepper|6|True|33
Pepper|7|True|Ketika nyawamu habis, kamu harus keluar medan tempur sampai pertempuran selesai.
Pepper|8|False|Pihak yang pertama mengeluarkan semua tentara lawan menang!
Penulis|9|False|Praktis.
Tentara|10|False|0
Pepper|11|True|?
Pepper|12|True|Hebat kan?
Pepper|13|True|Tidak ada lagi tentara yang terluka!
Pepper|14|False|Ini akan mengubah konsep perang kita selama ini!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Sekarang kita sudah tahu aturannya, mari ulang nyawanya dan mulai lagi dari awal!
Pepper|2|False|Supaya lebih adil.
Penulis|3|False|64
Penulis|4|False|45
Penulis|5|False|6
Penulis|6|False|2
Penulis|7|False|0
Penulis|8|False|0
Penulis|9|False|0
Penulis|10|False|0
Penulis|11|False|9
Penulis|12|False|5
Penulis|13|False|0
Penulis|14|False|0
Penulis|15|False|0
Suara|16|False|Sshing!
Suara|17|False|Zooo!
Suara|19|False|tchac!
Suara|18|False|poc!
Pepper|20|True|Lari lebih cepat, Carrot!
Pepper|21|False|Mantranya tidak akan bertahan lebih lama lagi!
Narator|22|False|- FIN -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|1|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|2|False|You can also translate this page if you want.
<hidden>|3|False|Beta readers help with the story, proofreaders give feedback about the text.
Kredit|4|False|June 29, 2020 Pelukis & Skenario: David Revoy. Penguji Bacaan Cerita: Arlo James Barnes, Craig Maloney, GunChleoc, Hyperbolic Pain, Karl Ove Hufthammer, Martin Disch, Nicolas Artance, Quetzal2, Valvin. Versi Bahasa Indonesia Penerjemah: Aldrian Obaja Muis . Berdasarkan alam semesta Hereva Pembuat: David Revoy. Penyunting utama: Craig Maloney. Penulis: Craig Maloney, Nartance, Scribblemaniac, Valvin. Pemeriksa: Willem Sonke, Moini, Hali, CGand, Alex Gryson . Peranti lunak: Krita 4.3, Inkscape 1.0 on Kubuntu 19.10. Izin (Lisensi): Creative Commons Attribution 4.0. www.peppercarrot.com
Pepper|5|True|Tahukah kamu?
Pepper|6|True|Pepper&Carrot sepenuhnya gratis (bebas), sumber terbuka dan disponsori oleh para pembacanya.
Pepper|7|False|Untuk episode ini, terima kasih kepada 1190 relawan!
Pepper|8|True|Anda juga bisa menjadi sukarelawan Pepper&Carrot dan dapatkan namamu di sini!
Pepper|9|True|Kami ada di Patreon, Tipeee, PayPal, Liberapay ...dan banyak lagi!
Pepper|10|True|Cek www.peppercarrot.com untuk informasi selanjutnya!
Pepper|11|False|Terima kasih!
