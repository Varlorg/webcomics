# Transcript of Pepper&Carrot Episode 06 [no]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Episode 6: Trylledrikk-konkurransen

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Åh søren, jeg må visst ha sovnet med vinduet åpent igjen...
Pepper|2|True|... det er så kjølig ...
Pepper|3|False|... og hvordan kan det ha seg at jeg ser Komona gjennom vinduet?
Pepper|4|False|KOMONA!
Pepper|5|False|Konkurransen!
Pepper|6|True|Jeg må ha... må ha sovnet ved et uhell!
Pepper|9|True|... men?
Pepper|10|False|Hvor er jeg ?!?
Bird|12|False|kK?|nowhitespace
Bird|11|True|kva|nowhitespace
Pepper|7|False|*
Note|8|False|* Se Episode 4 : Genibrygget

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|!!!
Pepper|2|False|Gulrot! Så søtt og omtenksomt av deg å fly meg til konkurransen!
Pepper|3|False|Fan-tas-tisk !
Pepper|4|True|Du har jo tilogmed husket å ta med en trylledrikk, klærne og hatten min...
Pepper|5|False|... nå får vi se hvilket brygg du valgte...

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|HVA POKKER ?!!
Mayor of Komona|3|False|Jeg, ordføreren av Komona, erklærer herved trylledrikk-konkurransen for åpnet!
Mayor of Komona|4|False|Det er en stor ære å for første gang kunne presentere hele fire deltagende hekser i denne konkurransen
Mayor of Komona|5|True|La oss gi en lang,
Mayor of Komona|6|True|STOR
Writing|2|False|Komona Trylledrikk-konkurranse
Mayor of Komona|7|False|applaus:

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Audience|29|False|klakk
Mayor of Komona|1|True|Helt fra de store Teknologers union, er det en stor ære å presentere henrivende og geniale
Mayor of Komona|3|True|... vi må heller ikke glemme vår lokale heks, Komonas egen
Mayor of Komona|5|True|... Fra månenedgangenes land har vi med oss vår tredje deltaker,
Mayor of Komona|7|True|... Og helt til slutt, fra Ekornhaleskogen, har vi med oss vår siste deltaker,
Mayor of Komona|2|False|Koriander !
Mayor of Komona|4|False|Safran !
Mayor of Komona|6|False|Shichimi !
Mayor of Komona|8|False|Pepper !
Mayor of Komona|9|True|La konkurransen begynne!
Mayor of Komona|10|False|Stemmene vil telles opp med applaus-o-meter
Mayor of Komona|11|False|Første oppvisning er det Koriander som står for
Koriander|13|False|... dere trenger ikke lenger å frykte døden takket være ...
Koriander|14|True|... mitt utrolige
Koriander|15|False|ZOMBIEBRYGG !
Audience|16|True|klakk
Audience|17|True|klakk
Audience|18|True|klakk
Audience|19|True|klakk
Audience|20|True|klakk
Audience|21|True|klakk
Audience|22|True|klakk
Audience|23|True|klakk
Audience|24|True|klakk
Audience|25|True|klakk
Audience|26|True|klakk
Audience|27|True|klakk
Audience|28|True|klakk
Koriander|12|False|Mine damer og herrer...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|FANTASTISK !
Audience|3|True|Klapp
Audience|4|True|Klapp
Audience|5|True|Klapp
Audience|6|True|klapp
Audience|7|True|Klapp
Audience|8|True|Klapp
Audience|9|True|Klapp
Audience|10|True|Klapp
Audience|11|True|Klapp
Audience|12|True|Klapp
Audience|13|True|Klapp
Audience|14|True|Klapp
Audience|15|True|Klapp
Audience|16|False|Klapp
Safran|18|True|for her er mitt
Safran|17|True|... spar på applausen folkens!
Safran|22|False|... og gjøre dem sjalu!
Safran|19|False|Brygg!
Safran|25|False|Brygg !
Safran|24|True|... SNOBBE -
Safran|23|False|... alt dette er mulig ved å påføre kun en enslig dråpe av mitt ...
Audience|26|True|Klapp
Audience|27|True|Klapp
Audience|28|True|Klapp
Audience|29|True|Klapp
Audience|30|True|Klapp
Audience|31|True|Klapp
Audience|32|True|Klapp
Audience|33|True|Klapp
Audience|34|True|Clap
Audience|35|True|Klapp
Audience|36|True|Klapp
Audience|37|True|Klapp
Audience|38|True|Klapp
Audience|39|True|Klapp
Audience|40|False|Klapp
Mayor of Komona|42|False|Denne eliksiren kan gjøre alle i Komona styrtrike!
Mayor of Komona|41|True|Fantastisk! Utrolig !
Audience|44|True|Klapp
Audience|45|True|Klapp
Audience|46|True|Klapp
Audience|47|True|Klapp
Audience|48|True|Klapp
Audience|49|True|Klapp
Audience|50|True|Klapp
Audience|51|True|Klapp
Audience|52|True|Klapp
Audience|53|True|Klapp
Audience|54|True|Klapp
Audience|55|True|klapp
Audience|56|True|Klapp
Audience|57|True|Klapp
Audience|58|True|Klapp
Audience|59|True|Klapp
Audience|60|False|Klapp
Mayor of Komona|2|False|Koriander sin mi-ra-ku-løse mikstur seirer over døden selv!
Safran|21|True|Den ordentlige trylledrikken dere alle har ventet på: Brygget som vil gi naboene hakeslepp ...
Mayor of Komona|43|False|Applausen deres er ikke til å ta feil av. Koriander er ute av konkurransen allerede

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|4|True|NEI !
Shichimi|5|True|Jeg kan ikke, det er for risikabelt
Shichimi|6|False|BEKLAGER !
Mayor of Komona|3|False|... Kom igjen nå Shichimi, alle venter kun på deg nå
Mayor of Komona|7|False|Det ser ut som Shichimi trekker seg mine damer og herrer...
Safran|8|False|Gi meg den !
Safran|9|False|... slutt å spille sjenert, du ødelegger showet
Safran|10|False|Alle vet dessuten at jeg har vunnet, uansett hva denne eliksiren din kan gjøre ...
Shichimi|11|False|!!!
Sound|12|False|B Z Z Z I IO O|nowhitespace
Shichimi|15|False|KJEMPEUHYRER !
Shichimi|2|False|Jeg.... jeg visste ikke at det var nødvendig å fremføre brygget
Shichimi|13|True|FORSIKTIG !!!
Shichimi|14|True|Trylledrikken skaper
Mayor of Komona|1|False|Det blir svært vanskelig å overgå siste fremføring for vår neste deltaker, Shichimi!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bird|1|False|KA-KAH-K aa aa aa aaa a a a|nowhitespace
Sound|2|False|BAM!
Pepper|3|True|... høh, kult !
Pepper|5|False|... brygget mitt burde i det minste bringe frem en god latter for ....
Pepper|4|False|er det min tur da ?
Mayor of Komona|6|True|Løp, din idiot!
Mayor of Komona|7|False|Konkurransen er avlyst! ...nå handler det kun om å overleve!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|2|False|... som vanlig forsvinner folk som dugg for sola når det er vår tur..
Pepper|1|True|Der ser du ...
Pepper|4|False|...f å alt tilbake til normalen og komme oss hjem igjen!
Pepper|6|True|Din
Pepper|7|False|forvokste zombi-kanari-snobb!
Pepper|9|False|Hypp på å prøve et siste brygg? ...
Pepper|10|False|... egentlig ikke, hæh ?
Pepper|5|False|HEI !
Sound|8|False|K N A S !|nowhitespace
Pepper|3|True|...men jeg har n å en liten tanken om hvordan vi kan gjøre nytte av "brygget" ditt, Gulrot

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Akkurat ja, les nøye på etiketten ...
Pepper|2|False|... Jeg nøler ikke et sekund med å helle hele innholdet over deg om ikke du kommer deg langt ut av Komona nå med det samme!
Mayor of Komona|3|True|Som takk for at hun reddet byen vår fra undergang
Mayor of Komona|4|False|g å r førstepremien til Pepper for sin trylledrikk som ... ??!!
Pepper|7|False|... eh... som faktisk ikke gjør så veldig mye.. ettersom det kun er en tisseprøve fra Gulrotpusen sitt siste veterinærbesøk! !
Pepper|6|True|... Haha! Jepp ...
Pepper|8|False|... ingen oppvisning ?...
Narrator|9|False|Episode 6 : Trylledrikk-konkurransen
Narrator|10|False|Slutt
Writing|5|False|50,000 Ko
Credits|11|False|Mars 2015 - Historie og Kunstnerisk utførelse av David Revoy - Oversatt til norsk av Thomas Nordstrøm

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Pepper&Gulrot er fullstendig fri, å pen kildekode og sponset med hjelp fra gavmilde lesere og "Patrons". Denne episoden er støttet av 245 Patrons, en stor takk til :
Credits|4|False|https://www.patreon.com/davidrevoy
Credits|3|False|Lyst til å bli en "Patron" og støtte Pepper&Gulrots neste episode? :
Credits|7|False|Verktøy : Denne episoden ble 100% tegnet med Fri/Libre programvare Krita på Linux Mint
Credits|6|False|Åpen kildekode : alle kildefiler og skrifttyper, er tilgjengelig på den offisielle nettsiden
Credits|5|False|Lisens : Creative Commons Attribution Du kan endre, dele videre, selge osv. ...
Credits|2|False|Алексей ★ Глеб Бузало ★ 獨孤欣 & 獨弧悦 ★ Addison Lewis ★ A Distinguished Robot ★ Adrian Lord ★ Ahmad Ali ★ Aina Reich ★ Alandran ★ Alan Hardman ★ Albert Westra ★ Alcide ★ Alejandro Flores Prieto ★ Alex ★ Alexander Bülow Tomassen ★ Alexander Sopicki ★ Alexandra Jordan ★ Alex Lusco ★ Alex Silver ★ Alex Vandiver ★ Alfredo ★ Ali Poulton (Aunty Pol) ★ Allan Zieser ★ Andreas Rieger ★ Andrej Kwadrin ★ Andrew ★ Andrew Godfrey ★ Andrey Alekseenko ★ Angela K ★ Anna Orlova ★ Antan Karmola ★ Anthony Edlin ★ Antonio Mendoza ★ Ardash Crowfoot ★ Arjun Chennu ★ Arne Brix ★ Aslak Kjølås-Sæverud ★ Axel Bordelon ★ Axel Philipsenburg ★ Barbix ★ Ben Evans ★ Bernd ★ Betsy Luntao ★ Boonsak Watanavisit ★ Boris Fauret ★ Boudewijn Rempt ★ BoxBoy ★ Brett Smith ★ Brian Behnke ★ Brian Smith ★ Bryan Butler ★ Bui Dang Hai Trieu ★ Carlos Levischi ★ Charlotte Lacombe-bar ★ Chris Radcliff ★ Chris Sakkas ★ Christian Gruenwaldner ★ Christophe Carré ★ Christopher Bates ★ Clara Dexter ★ codl ★ Colby Driedger ★ Conway Scott Smith ★ Cuthbert Williams ★ Cyrille Largillier ★ Cyril Paciullo ★ Damien ★ Daniel ★ Daniel Björkman ★ Danny Grimm ★ David Tang ★ DiCola Jamn ★ Dmitry ★ Donald Hayward ★ Duke ★ Eitan Goldshtrom ★ Enrico Billich ★ Epsilon ★ Eric Schulz ★ Faolan Grady ★ Francois Schnell ★ Garret Patterson ★ Ginny Hendricks ★ GreenAngel5 ★ Grigory Petrov ★ Guillaume ★ Guillaume Ballue ★ Gustav Strömbom ★ Guy Davis ★ Happy Mimic ★ Helmar Suschka ★ Henning Döscher ★ Ilyas ★ Irina Rempt ★ Ivan Korotkov ★ James Frazier ★ Janusz ★ Jared Tritsch ★ JDB ★ Jean-Baptiste Hebbrecht ★ Jean-Gabriel LOQUET ★ Jeffrey Schneider ★ Jessey Wright ★ Jim ★ Jim Street ★ Jiska ★ Joachim Schiele ★ JoÃ£o Luiz Machado Junior ★ Joern Konopka ★ joe rutledge ★ Johanne Thomson ★ John ★ John Urquhart Ferguson ★ Jónatan Nilsson ★ Jonathan Leroy ★ Jonathan Ringstad ★ Jon Brake ★ Jorge Bernal ★ Joseph Bowman ★ Julien Duroure ★ Justus Kat ★ Kai-Ting (Danil) Ko ★ Kasper Hansen ★ Kate ★ Kathryn Wuerstl ★ Ken Mingyuan Xia ★ Kingsquee ★ Kroet ★ Levi Kornelsen ★ Liselle ★ Lorentz Grip ★ L S ★ Luc Stepniewski ★ Luke Hochrein ★ MacCoy ★ Magnus Kronnäs ★ Manuel ★ Marc & Rick ★ Marcus ★ Martin Owens ★ Mary Brownlee ★ Masked Admirer ★ Mathias Stærk ★ Mefflin Ross Bullis-bates ★ Michael ★ Michael Gill ★ Michael Pureka ★ Michelle Pereira Garcia ★ Mike Mosher ★ Miroslav ★ Muzyka Dmytro ★ Nataya Castillo ★ Nazhif ★ Nicholas DeLateur ★ Nicholas Terranova ★ Nicki Aya ★ Nicola Angel ★ Nicolae Berbece ★ Nicole Heersema ★ Nielas Sinclair ★ NinjaKnight Comics ★ Noble Hays ★ Noelia Calles Marcos ★ Nora Czaykowski ★ Nyx ★ Olivier Amrein ★ Olivier Brun ★ Omar Willey ★ Oscar Moreno ★ Öykü Su Gürler ★ Ozone S. ★ Pablo Lopez Soriano ★ Pat David ★ Patrick Gamblin ★ Paul ★ Pavel Semenov ★ Pet0r ★ Peter ★ Peter Moonen ★ Petr Vlašic ★ Philippe Jean Edward Bateman ★ Pierre Geier ★ Pierre Vuillemin ★ Pranab Shenoy ★ Pyves & Ran ★ Raghavendra Kamath ★ Rajul Gupta ★ Reorx Meng ★ Ret Samys ★ Rictic ★ RJ van der Weide ★ Roberto Zaghis ★ Roman ★ Rumiko Hoshino ★ Rustin Simons ★ Sally Bridgewater ★ Sami T ★ Samuel Mitson ★ Scott Petrovic ★ Sean Adams ★ Shadefalcon ★ ShadowMist ★ Shafak ★ Shawn Meyer ★ Simon Forster ★ Simon Isenberg ★ Sonny W. ★ Soriac ★ Stanislav Vodetskyi ★ Stephanie Cheshire ★ Stephen Bates ★ Stephen Smoogen ★ Steven Bennett ★ Stuart Dickson ★ Surt ★ TamaskanLEM ★ Tar8156 ★ Terry Hancock ★ TheFaico ★ Thomas Citharel ★ Thomas Courbon ★ Thomas Schwery ★ Tim Burbank ★ Tim J. ★ Tomas Hajek ★ Tom Demian ★ Tom Savage ★ Tracey Reuben ★ Travis Humble ★ Tree ★ Tyson Tan ★ Urm ★ Victoria ★ Victoria White ★ Vladislav Kurdyukov ★ Vlad Tomash ★ Westen Curry ★ Witt N. Vest ★ Xavier Claude ★ Yalyn Vinkindo ★ Yaroslav ★ Zeni Pong ★ Źmicier Kušnaroŭ
