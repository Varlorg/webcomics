# Transcript of Pepper&Carrot Episode 06 [fi]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Episodi 6: Taikajuomakilpailu

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Pahus, unohdin taas sulkea ikkunan nukkumaankäydessä...
Pepper|2|True|... täällä tuulee ...
Pepper|3|False|... ja kuinka on mahdollista, että näen ikkunasta Komonan?
Pepper|4|False|KOMONA!
Pepper|5|False|Taikajuoma-kilpailu!
Pepper|6|True|Minun on täytynyt vahingossa nukahtaa!
Pepper|9|True|... mutta?
Pepper|10|False|Missä minä olen ?!?
Bird|12|False|Kvaak?|nowhitespace
Pepper|7|False|*|nowhitespace
Note|8|False|* Katso Episodi 4 : Neronleimaus

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|!!!
Pepper|2|False|Carrot! Olet suloinen, kun keksit viedä minut kilpailuun!
Pepper|3|False|Fan-tas-tis-ta !
Pepper|4|True|Tajusit myös ottaa mukaan taikajuomapullon, vaatteeni ja hattuni ...
Pepper|5|False|... katsotaanpa, minkä juoman otit...

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|MITÄ ?!!
Mayor of Komona|3|False|Komonan pormestarina julistan taikajuoma-kilpailun ... Avatuksi!
Mayor of Komona|4|False|Kaupunkimme on mielissään voidessaan toivottaa peräti neljä noitaa tervetulleeksi tähän ensimmäiseen kilpailuumme.
Mayor of Komona|5|True|Antakaahan
Mayor of Komona|6|True|raikuvat
Writing|2|False|Komonan Taikajuomakilpailu
Mayor of Komona|7|False|aplodit kullekin:

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Audience|29|False|Clap
Mayor of Komona|1|True|Meillä on kunnia toivottaa kaukaa Teknologien Unionista tervetulleeksi ihastuttava ja nerokas
Mayor of Komona|3|True|... unohtamatta kaupunkimme Komonan omaa noitaa:
Mayor of Komona|5|True|... Kolmas osanottaja tulee luoksemme laskevien kuiden maasta,
Mayor of Komona|7|True|... ja viimeinen osanottajamme saapuu Kurrenperän metsästä,
Mayor of Komona|2|False|Coriander !
Mayor of Komona|4|False|Saffron !
Mayor of Komona|6|False|Shichimi !
Mayor of Komona|8|False|Pepper !
Mayor of Komona|9|True|Kisa alkakoon!
Mayor of Komona|10|False|Voittaja ratkaistaan aplodien määrällä
Mayor of Komona|11|False|Aluksi Corianderin esitys
Coriander|13|False|...älkää enää pelätkö kuolemaa, sillä...
Coriander|14|True|... juomani on
Coriander|15|False|ZOMBIUTTAJA !
Audience|16|True|Clap
Audience|17|True|Clap
Audience|18|True|Clap
Audience|19|True|Clap
Audience|20|True|Clap
Audience|21|True|Clap
Audience|22|True|Clap
Audience|23|True|Clap
Audience|24|True|Clap
Audience|25|True|Clap
Audience|26|True|Clap
Audience|27|True|Clap
Audience|28|True|Clap
Coriander|12|False|Hyvät naiset ja herrat ...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|FANTASTISTA !
Audience|3|True|Clap
Audience|4|True|Clap
Audience|5|True|Clap
Audience|6|True|Clap
Audience|7|True|Clap
Audience|8|True|Clap
Audience|9|True|Clap
Audience|10|True|Clap
Audience|11|True|Clap
Audience|12|True|Clap
Audience|13|True|Clap
Audience|14|True|Clap
Audience|15|True|Clap
Audience|16|False|Clap
Saffron|18|True|sillä tässä on
Saffron|17|True|... mutta säästä-käähän taputuksianne, Komonan väki!
Saffron|22|False|... ja kateellisiksi!
Saffron|19|True|MINUN
Saffron|25|True|TYYLIKKYYS-
Saffron|24|True|...
Saffron|23|True|Siihen tarvitaan vain yksi tippa ...
Audience|27|True|Clap
Audience|28|True|Clap
Audience|29|True|Clap
Audience|30|True|Clap
Audience|31|True|Clap
Audience|32|True|Clap
Audience|33|True|Clap
Audience|34|True|Clap
Audience|35|True|Clap
Audience|36|True|Clap
Audience|37|True|Clap
Audience|38|True|Clap
Audience|39|True|Clap
Audience|40|True|Clap
Audience|41|False|Clap
Mayor of Komona|43|False|Tämän juoman avulla koko Komona voi rikastua!
Mayor of Komona|42|True|Fantastista! Uskomatonta!
Audience|45|True|Clap
Audience|46|True|Clap
Audience|47|True|Clap
Audience|48|True|Clap
Audience|49|True|Clap
Audience|50|True|Clap
Audience|51|True|Clap
Audience|52|True|Clap
Audience|53|True|Clap
Audience|54|True|Clap
Audience|55|True|Clap
Audience|56|True|Clap
Audience|57|True|Clap
Audience|58|True|Clap
Audience|59|True|Clap
Audience|60|True|Clap
Audience|61|False|Clap
Mayor of Komona|2|False|Coriander uhmaa itseänsä kuolemaa tällä ih-meel-li-sel-lä juomallaan!
Saffron|21|True|Tämä on se juoma, jota te kaikki olette odottaneet: se saa naapurinne hämmästymään ...
Mayor of Komona|44|False|Aplodien määrästä ei voi erehtyä. Coriander on jo pudonnut kilpailusta.
Saffron|20|False|juomaani !
Saffron|26|False|juomani

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|Edellisen esityksen lyöminen saattaa nyt olla vaikea tehtävä Shichimille!
Shichimi|4|True|EI !
Shichimi|5|True|En voi, se on liian vaarallista
Shichimi|6|False|OLEN PAHOILLANI
Mayor of Komona|3|False|... jouduhan, Shichimi, kaikki odottavat sinua
Mayor of Komona|7|False|Hyvät naiset ja herrat, näytää siltä, että Shichimi luovuttaa...
Saffron|8|False|Anna se minulle!
Saffron|9|False|... äläkä esitä ujoa, pilaat koko shown.
Saffron|10|False|Kaikkihan jo tietävät, että olen voittanut tämän kilpailun riippumatta siitä, mitä juomasi tekee ...
Shichimi|11|False|!!!
Sound|12|False|B Z Z Z I IO O|nowhitespace
Shichimi|15|False|JÄTTILÄISHIRVIÖN !
Shichimi|2|False|Minä... en tiennyt, että joudumme pitämään esitykset
Shichimi|13|True|VAROKAA!!!
Shichimi|14|True|Juoma tekee

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bird|1|False|CAW-CAW-C aa aa aa aa w w ww|nowhitespace
Sound|2|False|BAM!
Pepper|3|True|... siistiä !
Pepper|5|False|... juomani saa aikaan vähintäänkin hyvät naurut, sillä ...
Pepper|4|False|ja nyt on minun vuoroni
Mayor of Komona|7|False|Kilpailu on päättynyt! ... pelastaudu!
Mayor of Komona|6|True|Juokse, idiootti!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|2|False|... aina kaikki lähtevät juuri kun on meidän vuoromme
Pepper|1|True|näin siinä käy ...
Pepper|4|True|No, joka tapauksessa minulla on ajatus, mitä voimme tehdä "taikajuomallasi", Carrot
Pepper|5|False|...laitetaan asiat kuntoon täällä ja painutaan kotiin!
Pepper|7|True|Sinä
Pepper|8|False|ylösnoussut ylisuuri tyylitipu!
Pepper|10|False|Haluatko kokeilla vielä yhtä juomaa? ...
Pepper|11|False|... etkö todellakaan?
Pepper|6|False|HEI !
Sound|9|False|C R AC K !|nowhitespace

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Luehan tämä etiketti huolellisesti ...
Pepper|2|False|... en epäröi kaataa koko pullollista päällesi, jollet heti lähde tiehesi Komonasta!
Mayor of Komona|3|True|Koska hän pelasti kaupunkimme tuholta,
Mayor of Komona|4|False|annamme ensimmäisen palkinnon Pepperille hänen juomastaan, joka on ... ??!!
Pepper|7|False|... tuota ... itse asiassa tämä ei oikeastaan ole taikajuoma, vaan kissani pissanäyte viimeisimmältä eläinlääkärikäynniltä!
Pepper|6|True|... Hahhaa! niin ...
Pepper|8|False|... tarkempaa esittelyä ei kaivattane ?...
Narrator|9|False|Episodi 6 : Taikajuomakilpailu
Narrator|10|False|LOPPU
Writing|5|False|50,000 Ko
Credits|11|False|Maaliskuu 2015 - Piirrokset ja tarina: David Revoy - Käännös: Kari Lehto

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Pepper&Carrot on ilmainen, open source-tuote, jota lukijat ovat ystävällisesti tukeneet. Kiitokset tätä episodia sponsoroineille 245 tukijalle:
Credits|4|False|https://www.patreon.com/davidrevoy
Credits|3|False|Sinäkin voit ryhtyä tukemaan uusien Pepper&Carrot-jaksojen tekemistä:
Credits|7|False|Tämä episodi toteutettiin ilmaisilla open source-ohjelmistoilla Krita ja Linux Mint
Credits|6|False|Open source : kaikki lähdetiedostot tasoineen ja fontteineen löytyvät webbisivstolta
Credits|5|False|Lisenssi : Creative Commons Attribution Voit muokata, jakaa, myydä jne ...
Credits|2|False|Алексей ★ Глеб Бузало ★ 獨孤欣 & 獨弧悦 ★ Addison Lewis ★ A Distinguished Robot ★ Adrian Lord ★ Ahmad Ali ★ Aina Reich ★ Alandran ★ Alan Hardman ★ Albert Westra ★ Alcide ★ Alejandro Flores Prieto ★ Alex ★ Alexander Bülow Tomassen ★ Alexander Sopicki ★ Alexandra Jordan ★ Alex Lusco ★ Alex Silver ★ Alex Vandiver ★ Alfredo ★ Ali Poulton (Aunty Pol) ★ Allan Zieser ★ Andreas Rieger ★ Andrej Kwadrin ★ Andrew ★ Andrew Godfrey ★ Andrey Alekseenko ★ Angela K ★ Anna Orlova ★ Antan Karmola ★ Anthony Edlin ★ Antonio Mendoza ★ Ardash Crowfoot ★ Arjun Chennu ★ Arne Brix ★ Aslak Kjølås-Sæverud ★ Axel Bordelon ★ Axel Philipsenburg ★ Barbix ★ Ben Evans ★ Bernd ★ Betsy Luntao ★ Boonsak Watanavisit ★ Boris Fauret ★ Boudewijn Rempt ★ BoxBoy ★ Brett Smith ★ Brian Behnke ★ Brian Smith ★ Bryan Butler ★ Bui Dang Hai Trieu ★ Carlos Levischi ★ Charlotte Lacombe-bar ★ Chris Radcliff ★ Chris Sakkas ★ Christian Gruenwaldner ★ Christophe Carré ★ Christopher Bates ★ Clara Dexter ★ codl ★ Colby Driedger ★ Conway Scott Smith ★ Cuthbert Williams ★ Cyrille Largillier ★ Cyril Paciullo ★ Damien ★ Daniel ★ Daniel Björkman ★ Danny Grimm ★ David Tang ★ DiCola Jamn ★ Dmitry ★ Donald Hayward ★ Duke ★ Eitan Goldshtrom ★ Enrico Billich ★ Epsilon ★ Eric Schulz ★ Faolan Grady ★ Francois Schnell ★ Garret Patterson ★ Ginny Hendricks ★ GreenAngel5 ★ Grigory Petrov ★ Guillaume ★ Guillaume Ballue ★ Gustav Strömbom ★ Guy Davis ★ Happy Mimic ★ Helmar Suschka ★ Henning Döscher ★ Ilyas ★ Irina Rempt ★ Ivan Korotkov ★ James Frazier ★ Janusz ★ Jared Tritsch ★ JDB ★ Jean-Baptiste Hebbrecht ★ Jean-Gabriel LOQUET ★ Jeffrey Schneider ★ Jessey Wright ★ Jim ★ Jim Street ★ Jiska ★ Joachim Schiele ★ JoÃ£o Luiz Machado Junior ★ Joern Konopka ★ joe rutledge ★ Johanne Thomson ★ John ★ John Urquhart Ferguson ★ Jónatan Nilsson ★ Jonathan Leroy ★ Jonathan Ringstad ★ Jon Brake ★ Jorge Bernal ★ Joseph Bowman ★ Julien Duroure ★ Justus Kat ★ Kai-Ting (Danil) Ko ★ Kasper Hansen ★ Kate ★ Kathryn Wuerstl ★ Ken Mingyuan Xia ★ Kingsquee ★ Kroet ★ Levi Kornelsen ★ Liselle ★ Lorentz Grip ★ L S ★ Luc Stepniewski ★ Luke Hochrein ★ MacCoy ★ Magnus Kronnäs ★ Manuel ★ Marc & Rick ★ Marcus ★ Martin Owens ★ Mary Brownlee ★ Masked Admirer ★ Mathias Stærk ★ Mefflin Ross Bullis-bates ★ Michael ★ Michael Gill ★ Michael Pureka ★ Michelle Pereira Garcia ★ Mike Mosher ★ Miroslav ★ Muzyka Dmytro ★ Nataya Castillo ★ Nazhif ★ Nicholas DeLateur ★ Nicholas Terranova ★ Nicki Aya ★ Nicola Angel ★ Nicolae Berbece ★ Nicole Heersema ★ Nielas Sinclair ★ NinjaKnight Comics ★ Noble Hays ★ Noelia Calles Marcos ★ Nora Czaykowski ★ Nyx ★ Olivier Amrein ★ Olivier Brun ★ Omar Willey ★ Oscar Moreno ★ Öykü Su Gürler ★ Ozone S. ★ Pablo Lopez Soriano ★ Pat David ★ Patrick Gamblin ★ Paul ★ Pavel Semenov ★ Pet0r ★ Peter ★ Peter Moonen ★ Petr Vlašic ★ Philippe Jean Edward Bateman ★ Pierre Geier ★ Pierre Vuillemin ★ Pranab Shenoy ★ Pyves & Ran ★ Raghavendra Kamath ★ Rajul Gupta ★ Reorx Meng ★ Ret Samys ★ Rictic ★ RJ van der Weide ★ Roberto Zaghis ★ Roman ★ Rumiko Hoshino ★ Rustin Simons ★ Sally Bridgewater ★ Sami T ★ Samuel Mitson ★ Scott Petrovic ★ Sean Adams ★ Shadefalcon ★ ShadowMist ★ Shafak ★ Shawn Meyer ★ Simon Forster ★ Simon Isenberg ★ Sonny W. ★ Soriac ★ Stanislav Vodetskyi ★ Stephanie Cheshire ★ Stephen Bates ★ Stephen Smoogen ★ Steven Bennett ★ Stuart Dickson ★ Surt ★ TamaskanLEM ★ Tar8156 ★ Terry Hancock ★ TheFaico ★ Thomas Citharel ★ Thomas Courbon ★ Thomas Schwery ★ Tim Burbank ★ Tim J. ★ Tomas Hajek ★ Tom Demian ★ Tom Savage ★ Tracey Reuben ★ Travis Humble ★ Tree ★ Tyson Tan ★ Urm ★ Victoria ★ Victoria White ★ Vladislav Kurdyukov ★ Vlad Tomash ★ Westen Curry ★ Witt N. Vest ★ Xavier Claude ★ Yalyn Vinkindo ★ Yaroslav ★ Zeni Pong ★ Źmicier Kušnaroŭ
