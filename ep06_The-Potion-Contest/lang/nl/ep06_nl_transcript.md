# Transcript of Pepper&Carrot Episode 06 [nl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Aflevering 6: De toverdrankwedstrijd

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Verdorie, ik ben weer in slaap gevallen met het raam open …
Pepper|2|True|Het is hier zo koud …
Pepper|3|False|… en waarom zie ik opeens Komona door het raam?
Pepper|5|False|De toverdrank-wedstrijd!
Pepper|6|True|Ik moet per ongeluk in slaap gevallen zijn!
Pepper|9|True|Maar?
Pepper|10|False|Waar ben ik?
Bird|12|False|a k?|nowhitespace
Bird|11|True|K w a|nowhitespace
Pepper|7|False|*
Note|8|False|* Zie aflevering 4: Een geniaal moment
Pepper|4|False|KOMONA!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|!!!
Pepper|2|False|Carrot! Je hebt eraan gedacht me naar de wedstrijd te brengen! Wat lief!
Pepper|3|False|Fan-tas-tisch!
Pepper|4|True|Je hebt zelfs mijn kleren en mijn hoed meegenomen, en een toverdrank.
Pepper|5|False|Eens kijken welke drank dat is …

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|3|False|Als de burgemeester van Komona, verklaar ik de wedstrijd voor geopend!
Mayor of Komona|4|False|Ik voel me vereerd om maar liefst vier heksen te mogen verwelkomen voor deze eerste editie.
Mayor of Komona|5|False|Geef alstublieft een gigantisch applaus voor:
Writing|2|False|Toverdrankwedstrijd van Komona
Pepper|1|False|WAT?!!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Audience|29|False|Klap
Mayor of Komona|1|True|Ze komt helemaal vanuit de Technologistische Unie … Mag ik u voorstellen: de beeldschone en ingenieuze
Mayor of Komona|3|True|Niet te vergeten onze eigen Komonaanse deelneemster,
Mayor of Komona|5|True|Uit de landen van de ondergaande manen, is hier onze derde kandidate:
Mayor of Komona|7|True|… en ten slotte onze laatste deelneemster, afkomstig uit het Eikhoornwoud,
Mayor of Komona|2|False|Koriander!
Mayor of Komona|4|False|Saffraan!
Mayor of Komona|6|False|Shichimi!
Mayor of Komona|8|False|Pepper!
Mayor of Komona|9|True|Laat de wedstrijd beginnen!
Mayor of Komona|10|False|De winnaar zal worden bepaald door de applausmeter!
Mayor of Komona|11|False|Als eerste is hier Korianders demonstratie.
Koriander|13|False|… u hoeft de dood niet meer te vrezen, vanwege …
Koriander|14|True|… mijn Drank der
Koriander|15|False|ZOMBIFICATIE!
Audience|16|True|Klap
Audience|17|True|Klap
Audience|18|True|Klap
Audience|19|True|Klap
Audience|20|True|Klap
Audience|21|True|Klap
Audience|22|True|Klap
Audience|23|True|Klap
Audience|24|True|Klap
Audience|25|True|Klap
Audience|26|True|Klap
Audience|27|True|Klap
Audience|28|True|Klap
Koriander|12|False|Dames en heren …

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|True|FANTASTISCH!
Audience|3|True|Klap
Audience|4|True|Klap
Audience|5|True|Klap
Audience|6|True|Klap
Audience|7|True|Klap
Audience|8|True|Klap
Audience|9|True|Klap
Audience|10|True|Klap
Audience|11|True|Klap
Audience|12|True|Klap
Audience|13|True|Klap
Audience|14|True|Klap
Audience|15|True|Klap
Audience|16|False|Klap
Saffraan|18|True|want hier is
Saffraan|17|True|Maar spaar toch uw applaus, mensen van Komona,
Saffraan|21|False|… ze jaloers zal maken!
Saffraan|22|True|En dit alles door de simpele aanbrenging van een enkel drupje van mijn …
Audience|24|True|Klap
Audience|25|True|Klap
Audience|26|True|Klap
Audience|27|True|Klap
Audience|28|True|Klap
Audience|29|True|Klap
Audience|30|True|Klap
Audience|31|True|Klap
Audience|32|True|klap
Audience|33|True|Klap
Audience|34|True|Klap
Audience|35|True|Klap
Audience|36|True|Klap
Audience|37|True|Klap
Audience|38|False|Klap
Mayor of Komona|40|False|Deze drank kan heel Komona rijk maken!
Mayor of Komona|39|True|Fantastisch! Ongelofelijk!
Audience|42|True|Klap
Audience|43|True|Klap
Audience|44|True|Klap
Audience|45|True|Klap
Audience|46|True|Klap
Audience|47|True|Klap
Audience|48|True|Klap
Audience|49|True|Klap
Audience|50|True|Klap
Audience|51|True|Klap
Audience|52|True|Klap
Audience|53|True|Klap
Audience|54|True|Klap
Audience|55|True|Klap
Audience|56|True|Klap
Audience|57|True|Klap
Audience|58|False|Klap
Mayor of Komona|2|False|Koriander verslaat eigenhandig de dood met deze mi-ra-cu-leuze drank!
Saffraan|20|True|De toverdrank waar u al zo lang naar op zoek bent: de drank die uw buren zal verbazen …
Mayor of Komona|41|False|Uw applaus geeft de doorslag … Koriander is reeds uitgeschakeld.
Saffraan|19|False|MIJN drank!
Saffraan|23|False|… Drank der OVERDAAD!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|Deze demonstratie is vast moeilijk te verslaan voor Shichimi!
Shichimi|4|True|N e e!|nowhitespace
Shichimi|5|True|Dat gaat niet, het is te gevaarlijk.
Shichimi|6|False|SORRY!
Mayor of Komona|3|False|Kom op Shichimi, iedereen wacht op je!
Mayor of Komona|7|False|Dames en heren, het lijkt erop dat Shichimi zich terugtrekt.
Saffraan|8|False|Geef hier!
Saffraan|10|False|Het is toch al duidelijk dat ik ga winnen, wat jouw drankje ook moge doen …
Shichimi|11|False|!!!
Sound|12|False|B Z Z Z I IEE|nowhitespace
Shichimi|2|False|Ik … Ik wist niet dat we een demonstratie moesten geven …
Shichimi|14|True|Het is een drank voor
Saffraan|9|False|… en hou op met dat aanstellerige gedoe. Je verpest de show.
Shichimi|13|True|PAS OP!!!
Shichimi|15|False|ENORME MONSTERS!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bird|1|False|KRAKRA KRRR A AA AA A A AA|nowhitespace
Sound|2|False|BAM!
Pepper|4|True|Dus nu mag ik?
Pepper|3|True|… leuk!
Pepper|5|False|Ik hoop dat jullie mijn toverdrank kunnen waarderen …
Mayor of Komona|6|True|Ren, idioot!
Mayor of Komona|7|False|De wedstrijd is afgelopen! Red jezelf!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Moet je zien …
Sound|7|False|K R AA K!|nowhitespace
Pepper|2|False|… zoals gebruikelijk vertrekt iedereen net als je aan de beurt bent.
Pepper|3|True|Ik weet nu tenminste wat ik met jouw “toverdrank" moet doen, Carrot …
Pepper|4|False|… hier alles weer in orde maken en terug naar huis gaan!
Pepper|6|False|Jij overmaatse l u x e - zombi e-kanarie!
Pepper|5|False|HÉ!
Pepper|8|False|Nog één drankje proberen?
Pepper|9|False|… liever niet, hè?

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrator|9|False|Aflevering 6: De toverdrankwedstrijd
Writing|5|False|50 000 Ko
Pepper|1|True|Ja, lees het etiket maar eens goed …
Pepper|2|False|Als je niet direct maakt dat je wegkomt, dan giet ik dit zonder twijfelen over je heen!
Mayor of Komona|3|True|Omdat ze onze stad uit een groot gevaar gered heeft,
Pepper|7|False|… ehm … Eigenlijk is het helemaal geen toverdrank … Het is een staaltje kattenpis, van een bezoek aan de dierenarts!
Pepper|8|False|… zal ik de demonstratie maar achterwege laten?
Narrator|10|False|EINDE
Mayor of Komona|4|False|reiken we de eerste plaats uit aan Pepper voor haar drank der … eh …?
Credits|11|False|Maart 2015 — Tekeningen en verhaal: David Revoy — Vertaling: Willem Sonke, met verbeteringen door Midgard
Pepper|6|True|… Haha! Tja …

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Pepper&Carrot is geheel vrij en open-bron door de hulp van mijn patrons. Voor deze aflevering bedank ik mijn 245 patrons:
Credits|4|False|https://www.patreon.com/davidrevoy
Credits|3|False|Voor de volgende aflevering kun jij ook een patron van Pepper&Carrot worden:
Credits|7|False|Software: deze aflevering is voor 100% gemaakt met vrije software Krita op Linux Mint
Credits|6|False|Open-bron: alle bronbestanden met lagen en zetwerk zijn beschikbaar op de officiële site
Credits|5|False|Licentie: Creative Commons Naamsvermelding Aanpassen, delen, verkopen, enz. is toegestaan
Credits|2|False|Алексей ★ Глеб Бузало ★ 獨孤欣 & 獨弧悦 ★ Addison Lewis ★ A Distinguished Robot ★ Adrian Lord ★ Ahmad Ali ★ Aina Reich ★ Alandran ★ Alan Hardman ★ Albert Westra ★ Alcide ★ Alejandro Flores Prieto ★ Alex ★ Alexander Bülow Tomassen ★ Alexander Sopicki ★ Alexandra Jordan ★ Alex Lusco ★ Alex Silver ★ Alex Vandiver ★ Alfredo ★ Ali Poulton (Aunty Pol) ★ Allan Zieser ★ Andreas Rieger ★ Andrej Kwadrin ★ Andrew ★ Andrew Godfrey ★ Andrey Alekseenko ★ Angela K ★ Anna Orlova ★ Antan Karmola ★ Anthony Edlin ★ Antonio Mendoza ★ Ardash Crowfoot ★ Arjun Chennu ★ Arne Brix ★ Aslak Kjølås-Sæverud ★ Axel Bordelon ★ Axel Philipsenburg ★ Barbix ★ Ben Evans ★ Bernd ★ Betsy Luntao ★ Boonsak Watanavisit ★ Boris Fauret ★ Boudewijn Rempt ★ BoxBoy ★ Brett Smith ★ Brian Behnke ★ Brian Smith ★ Bryan Butler ★ Bui Dang Hai Trieu ★ Carlos Levischi ★ Charlotte Lacombe-bar ★ Chris Radcliff ★ Chris Sakkas ★ Christian Gruenwaldner ★ Christophe Carré ★ Christopher Bates ★ Clara Dexter ★ codl ★ Colby Driedger ★ Conway Scott Smith ★ Cuthbert Williams ★ Cyrille Largillier ★ Cyril Paciullo ★ Damien ★ Daniel ★ Daniel Björkman ★ Danny Grimm ★ David Tang ★ DiCola Jamn ★ Dmitry ★ Donald Hayward ★ Duke ★ Eitan Goldshtrom ★ Enrico Billich ★ Epsilon ★ Eric Schulz ★ Faolan Grady ★ Francois Schnell ★ Garret Patterson ★ Ginny Hendricks ★ GreenAngel5 ★ Grigory Petrov ★ Guillaume ★ Guillaume Ballue ★ Gustav Strömbom ★ Guy Davis ★ Happy Mimic ★ Helmar Suschka ★ Henning Döscher ★ Ilyas ★ Irina Rempt ★ Ivan Korotkov ★ James Frazier ★ Janusz ★ Jared Tritsch ★ JDB ★ Jean-Baptiste Hebbrecht ★ Jean-Gabriel LOQUET ★ Jeffrey Schneider ★ Jessey Wright ★ Jim ★ Jim Street ★ Jiska ★ Joachim Schiele ★ JoÃ£o Luiz Machado Junior ★ Joern Konopka ★ joe rutledge ★ Johanne Thomson ★ John ★ John Urquhart Ferguson ★ Jónatan Nilsson ★ Jonathan Leroy ★ Jonathan Ringstad ★ Jon Brake ★ Jorge Bernal ★ Joseph Bowman ★ Julien Duroure ★ Justus Kat ★ Kai-Ting (Danil) Ko ★ Kasper Hansen ★ Kate ★ Kathryn Wuerstl ★ Ken Mingyuan Xia ★ Kingsquee ★ Kroet ★ Levi Kornelsen ★ Liselle ★ Lorentz Grip ★ L S ★ Luc Stepniewski ★ Luke Hochrein ★ MacCoy ★ Magnus Kronnäs ★ Manuel ★ Marc & Rick ★ Marcus ★ Martin Owens ★ Mary Brownlee ★ Masked Admirer ★ Mathias Stærk ★ Mefflin Ross Bullis-bates ★ Michael ★ Michael Gill ★ Michael Pureka ★ Michelle Pereira Garcia ★ Mike Mosher ★ Miroslav ★ Muzyka Dmytro ★ Nataya Castillo ★ Nazhif ★ Nicholas DeLateur ★ Nicholas Terranova ★ Nicki Aya ★ Nicola Angel ★ Nicolae Berbece ★ Nicole Heersema ★ Nielas Sinclair ★ NinjaKnight Comics ★ Noble Hays ★ Noelia Calles Marcos ★ Nora Czaykowski ★ Nyx ★ Olivier Amrein ★ Olivier Brun ★ Omar Willey ★ Oscar Moreno ★ Öykü Su Gürler ★ Ozone S. ★ Pablo Lopez Soriano ★ Pat David ★ Patrick Gamblin ★ Paul ★ Pavel Semenov ★ Pet0r ★ Peter ★ Peter Moonen ★ Petr Vlašic ★ Philippe Jean Edward Bateman ★ Pierre Geier ★ Pierre Vuillemin ★ Pranab Shenoy ★ Pyves & Ran ★ Raghavendra Kamath ★ Rajul Gupta ★ Reorx Meng ★ Ret Samys ★ Rictic ★ RJ van der Weide ★ Roberto Zaghis ★ Roman ★ Rumiko Hoshino ★ Rustin Simons ★ Sally Bridgewater ★ Sami T ★ Samuel Mitson ★ Scott Petrovic ★ Sean Adams ★ Shadefalcon ★ ShadowMist ★ Shafak ★ Shawn Meyer ★ Simon Forster ★ Simon Isenberg ★ Sonny W. ★ Soriac ★ Stanislav Vodetskyi ★ Stephanie Cheshire ★ Stephen Bates ★ Stephen Smoogen ★ Steven Bennett ★ Stuart Dickson ★ Surt ★ TamaskanLEM ★ Tar8156 ★ Terry Hancock ★ TheFaico ★ Thomas Citharel ★ Thomas Courbon ★ Thomas Schwery ★ Tim Burbank ★ Tim J. ★ Tomas Hajek ★ Tom Demian ★ Tom Savage ★ Tracey Reuben ★ Travis Humble ★ Tree ★ Tyson Tan ★ Urm ★ Victoria ★ Victoria White ★ Vladislav Kurdyukov ★ Vlad Tomash ★ Westen Curry ★ Witt N. Vest ★ Xavier Claude ★ Yalyn Vinkindo ★ Yaroslav ★ Zeni Pong ★ Źmicier Kušnaroŭ
