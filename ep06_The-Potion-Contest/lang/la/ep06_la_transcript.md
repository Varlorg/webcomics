# Transcript of Pepper&Carrot Episode 06 [la]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Episodium 6 : Agon potionum

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Ei ! Iterum obdormivi aperta fenestra…
Pepper|2|True|… ac qualem ventum !
Pepper|3|False|… Et cur video Komonam per fenestram ?
Pepper|4|False|KOMONA !
Pepper|5|False|Agon potionum !
Pepper|6|True|Me… me casu obdormisse, credo !
Pepper|9|True|… At ?
Pepper|10|False|Ubi sum ?!?
Bird|12|False|x ?|nowhitespace
Bird|11|True|qua|nowhitespace
Pepper|7|False|*
Note|8|False|* Vide episodium 4 : Repentinum ingenium

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|!!!
Pepper|2|False|Carota ! Quam dulce quod recordatus es ut in agonem me ducas !
Pepper|3|False|Mi-ri-fi-ce !
Pepper|4|True|Recordatus es etiam ut capias potionem aliquam, atque vestes et galerum mea…
Pepper|5|False|… Videamus quam potionem ceperis…

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|QUID ?!!
Mayor of Komona|3|False|Ego praefectus urbis Komonae, denuntio agonem potionum inceptum !
Mayor of Komona|4|False|Urbs nostra valde laetatur se accipere nihil minus quam quattuor magas in hoc primo agone.
Mayor of Komona|5|True|Peto ut plaudatis
Mayor of Komona|6|False|magno cum strepitu :
Writing|2|False|Agon Potionum Komonalis

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Audience|30|False|Clap
Mayor of Komona|1|True|Venientem e magna Unione Technologistarum, nos honestat accipere pulchram et ingeniosam
Mayor of Komona|3|True|… Nec obliviscamur puellae ingenuae, ipsius magae Komonalis,
Mayor of Komona|5|True|… Nostra tertia particeps venit e tractu cadentium lunarum,
Mayor of Komona|7|True|… Postremo, nostra ultima particeps, e Sciurifini silva veniens,
Mayor of Komona|2|False|Coriandrum !
Mayor of Komona|4|False|Croci !
Mayor of Komona|6|False|Shichimi !
Mayor of Komona|8|False|Piper !
Mayor of Komona|9|True|Incipiat agon !
Mayor of Komona|10|False|Victricem eligent plausus !
Mayor of Komona|11|False|Et primo, en demonstratio Coriandri.
Coriander|13|False|Dominae et Domini…
Coriander|14|True|… nolite jam timere mortem propter…
Coriander|15|True|… potionem meam ad
Coriander|16|False|ZOMBIFICANDUM !
Audience|17|True|Clap
Audience|18|True|Clap
Audience|19|True|Clap
Audience|20|True|Clap
Audience|21|True|Clap
Audience|22|True|Clap
Audience|23|True|Clap
Audience|24|True|Clap
Audience|25|True|Clap
Audience|26|True|Clap
Audience|27|True|Clap
Audience|28|True|Clap
Audience|29|True|Clap

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|MIRIFICE !
Mayor of Komona|2|False|Coriandrum lacessat ipsam mortem hac cum potione per-mi-ra !
Audience|4|True|Clap
Audience|5|True|Clap
Audience|6|True|Clap
Audience|7|True|Clap
Audience|8|True|Clap
Audience|9|True|Clap
Audience|10|True|Clap
Audience|11|True|Clap
Audience|12|True|Clap
Audience|13|True|Clap
Audience|14|True|Clap
Audience|15|True|Clap
Audience|16|True|Clap
Audience|17|False|Clap
Saffron|19|True|Nam ecce
Saffron|18|False|… At quaeso, servate plausus vestros, Quirites Komonae !
Saffron|22|True|Potionem illam quam omnes sperabatis : quae commovebit vicinos omnes vestros…
Saffron|23|False|… eos faciet invidiosos !
Saffron|20|True|MEA
Saffron|26|False|URBANITATIS !
Saffron|25|True|… potionis
Saffron|24|True|… Hoc omne jam potestis, si utimini una gutta meae…
Audience|28|True|Clap
Audience|29|True|Clap
Audience|30|True|Clap
Audience|31|True|Clap
Audience|32|True|Clap
Audience|33|True|Clap
Audience|34|True|Clap
Audience|35|True|Clap
Audience|36|True|Clap
Audience|37|True|Clap
Audience|38|True|Clap
Audience|39|True|Clap
Audience|40|True|Clap
Audience|42|False|Clap
Audience|41|True|Clap
Mayor of Komona|44|False|Haec potio possit facere omnis Komona divis !
Mayor of Komona|43|True|Mirum ! Incredibile !
Audience|46|True|Clap
Audience|47|True|Clap
Audience|48|True|Clap
Audience|49|True|Clap
Audience|50|True|Clap
Audience|51|True|Clap
Audience|52|True|Clap
Audience|53|True|Clap
Audience|54|True|Clap
Audience|55|True|Clap
Audience|56|True|Clap
Audience|57|True|Clap
Audience|58|True|Clap
Audience|59|True|Clap
Audience|60|False|Clap
Audience|3|True|Clap
Audience|27|True|Clap
Mayor of Komona|45|False|Plausus vestri neqeunt id manifestius affirmare : Coriandrum jam dimissa est.
Saffron|21|False|potio !

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|Nunc erit difficile ut Shichimi aemuletur demonstrationem illam !
Shichimi|4|True|NON !
Shichimi|5|True|Nequeo, nimis periculosum.
Shichimi|6|False|VENIAM PETO !
Mayor of Komona|3|False|Agedum, Shichimi, omnes sperant a te.
Mayor of Komona|7|False|Dominae et Domini, videtur Shichimi deserere…
Saffron|8|False|Da mi !
Saffron|9|False|… Desineque agere quasi es timida et corrumpere spectaculum.
Saffron|10|False|Omnes sciunt me jam in agone vicisse, quoquo agat potio tua…
Shichimi|11|False|!!!
Sound|12|False|B Z Z Z I IO O|nowhitespace
Shichimi|15|False|INGENTIS FERAE !
Shichimi|2|False|Ne… nesciebam me oportere demonstrare potionem.
Shichimi|13|True|CAVETE !!!
Shichimi|14|True|Haec est potio

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bird|1|False|CHAC-CH ac-c a a a aa a c|nowhitespace
Sound|2|False|BAM!
Pepper|3|True|… hem, bene !
Pepper|5|False|… Potio mea vobis erit saltem ridicula, quia…
Pepper|4|False|Nunc ego sum actura ?
Mayor of Komona|6|True|Fuge, stulta !
Mayor of Komona|7|False|Agon desitus est ! … Serva te !

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|2|False|.… ut solet, omnes abeunt cum nostrum est tempus agendi.
Pepper|1|True|En ergo…
Pepper|4|True|Saltem, mihi advenit in mentem quid acturi simus de « potione » tua Carota…
Pepper|5|False|… componamus res ibi et redeamus !
Pepper|7|True|Tu
Pepper|8|False|Ingens-zombi-urbanus-aviculus !
Pepper|10|False|Tibi placeret probare ultimam potionem… ?
Pepper|11|False|… Nonne immo ?
Pepper|6|False|EHO !
Sound|9|False|C R AC !|nowhitespace

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Ita, lege pittacium…
Pepper|2|False|… Non diu dubitabo eo de in te fundendo nisi statim e Komona exis !
Mayor of Komona|3|True|Quia urbem nostram e clade aliqua servavit,
Mayor of Komona|4|False|denuntiamus Piperem victricem propter potionem suam… ??!!
Pepper|7|False|… Hem… Re vera, nulla vera potio est ; specimina sunt urinae catti mei a tempore quo nuper ad medicum visit !
Pepper|6|True|… Haha ! verumst …
Pepper|8|False|… Vobis non demonstro, ne… ?
Narrator|9|False|Episodium 6 : Agon potionum
Narrator|10|False|FINIS
Writing|5|False|50 000 Ko
Credits|11|False|Martiis 2015 - Designum et fabula : David Revoy, conversio in Latinum : Guillaume Lestringant

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Piper&Carota gratis constat, omnno apertum fontis est ac juvatur maecenatu lectorum ; in hoc episodium, gratias ago illis 245 maecenatibus :
Credits|4|False|https://www.patreon.com/davidrevoy
Credits|3|False|Tu quoque, potes fieri maecenas Piperis&Carotae sequenti episodio :
Credits|7|False|Instrumenta : Hoc episodium omnino designatum est liberis programmatibus Krita in Linux Mint
Credits|6|False|Apertum fontis : omnia fontes, genera litterarum, imagines stratatae possunt discarricari in pagina interretiali.
Credits|5|False|Licentia : Creative Commons Attribution potes mutare, communicare, vendere, etc.
Credits|2|False|Алексей ★ Глеб Бузало ★ 獨孤欣 & 獨弧悦 ★ Addison Lewis ★ A Distinguished Robot ★ Adrian Lord ★ Ahmad Ali ★ Aina Reich ★ Alandran ★ Alan Hardman ★ Albert Westra ★ Alcide ★ Alejandro Flores Prieto ★ Alex ★ Alexander Bülow Tomassen ★ Alexander Sopicki ★ Alexandra Jordan ★ Alex Lusco ★ Alex Silver ★ Alex Vandiver ★ Alfredo ★ Ali Poulton (Aunty Pol) ★ Allan Zieser ★ Andreas Rieger ★ Andrej Kwadrin ★ Andrew ★ Andrew Godfrey ★ Andrey Alekseenko ★ Angela K ★ Anna Orlova ★ Antan Karmola ★ Anthony Edlin ★ Antonio Mendoza ★ Ardash Crowfoot ★ Arjun Chennu ★ Arne Brix ★ Aslak Kjølås-Sæverud ★ Axel Bordelon ★ Axel Philipsenburg ★ Barbix ★ Ben Evans ★ Bernd ★ Betsy Luntao ★ Boonsak Watanavisit ★ Boris Fauret ★ Boudewijn Rempt ★ BoxBoy ★ Brett Smith ★ Brian Behnke ★ Brian Smith ★ Bryan Butler ★ Bui Dang Hai Trieu ★ Carlos Levischi ★ Charlotte Lacombe-bar ★ Chris Radcliff ★ Chris Sakkas ★ Christian Gruenwaldner ★ Christophe Carré ★ Christopher Bates ★ Clara Dexter ★ codl ★ Colby Driedger ★ Conway Scott Smith ★ Cuthbert Williams ★ Cyrille Largillier ★ Cyril Paciullo ★ Damien ★ Daniel ★ Daniel Björkman ★ Danny Grimm ★ David Tang ★ DiCola Jamn ★ Dmitry ★ Donald Hayward ★ Duke ★ Eitan Goldshtrom ★ Enrico Billich ★ Epsilon ★ Eric Schulz ★ Faolan Grady ★ Francois Schnell ★ Garret Patterson ★ Ginny Hendricks ★ GreenAngel5 ★ Grigory Petrov ★ Guillaume ★ Guillaume Ballue ★ Gustav Strömbom ★ Guy Davis ★ Happy Mimic ★ Helmar Suschka ★ Henning Döscher ★ Ilyas ★ Irina Rempt ★ Ivan Korotkov ★ James Frazier ★ Janusz ★ Jared Tritsch ★ JDB ★ Jean-Baptiste Hebbrecht ★ Jean-Gabriel LOQUET ★ Jeffrey Schneider ★ Jessey Wright ★ Jim ★ Jim Street ★ Jiska ★ Joachim Schiele ★ JoÃ£o Luiz Machado Junior ★ Joern Konopka ★ joe rutledge ★ Johanne Thomson ★ John ★ John Urquhart Ferguson ★ Jónatan Nilsson ★ Jonathan Leroy ★ Jonathan Ringstad ★ Jon Brake ★ Jorge Bernal ★ Joseph Bowman ★ Julien Duroure ★ Justus Kat ★ Kai-Ting (Danil) Ko ★ Kasper Hansen ★ Kate ★ Kathryn Wuerstl ★ Ken Mingyuan Xia ★ Kingsquee ★ Kroet ★ Levi Kornelsen ★ Liselle ★ Lorentz Grip ★ L S ★ Luc Stepniewski ★ Luke Hochrein ★ MacCoy ★ Magnus Kronnäs ★ Manuel ★ Marc & Rick ★ Marcus ★ Martin Owens ★ Mary Brownlee ★ Masked Admirer ★ Mathias Stærk ★ Mefflin Ross Bullis-bates ★ Michael ★ Michael Gill ★ Michael Pureka ★ Michelle Pereira Garcia ★ Mike Mosher ★ Miroslav ★ Muzyka Dmytro ★ Nataya Castillo ★ Nazhif ★ Nicholas DeLateur ★ Nicholas Terranova ★ Nicki Aya ★ Nicola Angel ★ Nicolae Berbece ★ Nicole Heersema ★ Nielas Sinclair ★ NinjaKnight Comics ★ Noble Hays ★ Noelia Calles Marcos ★ Nora Czaykowski ★ Nyx ★ Olivier Amrein ★ Olivier Brun ★ Omar Willey ★ Oscar Moreno ★ Öykü Su Gürler ★ Ozone S. ★ Pablo Lopez Soriano ★ Pat David ★ Patrick Gamblin ★ Paul ★ Pavel Semenov ★ Pet0r ★ Peter ★ Peter Moonen ★ Petr Vlašic ★ Philippe Jean Edward Bateman ★ Pierre Geier ★ Pierre Vuillemin ★ Pranab Shenoy ★ Pyves & Ran ★ Raghavendra Kamath ★ Rajul Gupta ★ Reorx Meng ★ Ret Samys ★ Rictic ★ RJ van der Weide ★ Roberto Zaghis ★ Roman ★ Rumiko Hoshino ★ Rustin Simons ★ Sally Bridgewater ★ Sami T ★ Samuel Mitson ★ Scott Petrovic ★ Sean Adams ★ Shadefalcon ★ ShadowMist ★ Shafak ★ Shawn Meyer ★ Simon Forster ★ Simon Isenberg ★ Sonny W. ★ Soriac ★ Stanislav Vodetskyi ★ Stephanie Cheshire ★ Stephen Bates ★ Stephen Smoogen ★ Steven Bennett ★ Stuart Dickson ★ Surt ★ TamaskanLEM ★ Tar8156 ★ Terry Hancock ★ TheFaico ★ Thomas Citharel ★ Thomas Courbon ★ Thomas Schwery ★ Tim Burbank ★ Tim J. ★ Tomas Hajek ★ Tom Demian ★ Tom Savage ★ Tracey Reuben ★ Travis Humble ★ Tree ★ Tyson Tan ★ Urm ★ Victoria ★ Victoria White ★ Vladislav Kurdyukov ★ Vlad Tomash ★ Westen Curry ★ Witt N. Vest ★ Xavier Claude ★ Yalyn Vinkindo ★ Yaroslav ★ Zeni Pong ★ Źmicier Kušnaroŭ
