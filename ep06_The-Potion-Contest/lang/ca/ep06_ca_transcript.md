# Transcript of Pepper&Carrot Episode 06 [ca]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Episodi 6: El torneig de pocions

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Càsum...m'he tornat a adormir amb la finestra oberta...
Pepper|2|True|...quina ventada...
Pepper|3|False|...i com és que puc veure Komona per la finestra?
Pepper|4|False|KOMONA!
Pepper|5|False|El torneig de pocions!
Pepper|6|True|Em dec... Em dec haver quedat clapada sense voler!
Pepper|9|True|...però?
Pepper|10|False|On sóc?!?
Bird|12|False|cK?|nowhitespace
Bird|11|True|qua|nowhitespace
Pepper|7|False|*
Note|8|False|*Veure episodi 4 : Cop de genialitat

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|!!!
Pepper|2|False|Carrot! Que n'ets de bufó d'haver pensat en portar-me al torneig!
Pepper|3|False|Fan-tàs-tic !
Pepper|4|True|Fins i tot has pensat a agafar una poció, la roba i el meu barret ...
Pepper|5|False|...a veure quina poció has agafat...

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|QUÈ?!!
Mayor of Komona|3|False|Com a batlle de Komona, declaro aquest torneig de pocions... Inaugurat!
Mayor of Komona|4|False|La nostra vila està encantada de donar la benvinguda a gens menys que a quatre bruixes per aquesta primera edició!
Mayor of Komona|5|True|Si us plau, un
Mayor of Komona|6|True|fort
Writing|2|False|Torneig de pocions
Mayor of Komona|7|False|aplaudiment per:

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Audience|29|False|Clap
Mayor of Komona|1|True|Provinent de la Gran Unió Tecnològica, és un honor donar la benginvuda a l'encantadora i ingeniosa
Mayor of Komona|3|True|...sense oblidar la nostra campiona, la gran bruixa de Komona,
Mayor of Komona|5|True|...la nostra tercera participant ve des de les Terres de les postes de llunes,
Mayor of Komona|7|True|..i finalment, la nostra última participant, des del bosc de Cap de l'esquirol,
Mayor of Komona|2|False|Coriander !
Mayor of Komona|4|False|Saffron !
Mayor of Komona|6|False|Shichimi !
Mayor of Komona|8|False|Pepper !
Mayor of Komona|9|True|Que comencin els jocs!
Mayor of Komona|10|False|La votació tindrà lloc mitjançant l'aplaudímetre!
Mayor of Komona|11|False|Començarem per la demostració de la Coriander!
Coriander|13|False|...deixin de tèmer a la mort, gràcies a la meva...
Coriander|14|True|...poció de
Coriander|15|False|ZOMBIFICACIÓ!
Audience|16|True|Clap
Audience|17|True|Clap
Audience|18|True|Clap
Audience|19|True|Clap
Audience|20|True|Clap
Audience|21|True|Clap
Audience|22|True|Clap
Audience|23|True|Clap
Audience|24|True|Clap
Audience|25|True|Clap
Audience|26|True|Clap
Audience|27|True|Clap
Audience|28|True|Clap
Coriander|12|False|Dames i cavallers...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|FANTÀSTIC !
Audience|3|True|Clap
Audience|4|True|Clap
Audience|5|True|Clap
Audience|6|True|Clap
Audience|7|True|Clap
Audience|8|True|Clap
Audience|9|True|Clap
Audience|10|True|Clap
Audience|11|True|Clap
Audience|12|True|Clap
Audience|13|True|Clap
Audience|14|True|Clap
Audience|15|True|Clap
Audience|16|False|Clap
Saffron|18|True|Aquí tenen la
Saffron|17|True|...si us plau, esperin abans d'aplaudir, gents de Komona!
Saffron|22|False|...tots es moriran d'enveja!
Saffron|19|True|MEVA
Saffron|25|False|PATXOCA!
Saffron|24|True|...poció de
Saffron|23|False|...tot això serà possible gràcies a una simple goteta de la meva...
Audience|26|True|Clap
Audience|27|True|Clap
Audience|28|True|Clap
Audience|29|True|Clap
Audience|30|True|Clap
Audience|31|True|Clap
Audience|32|True|Clap
Audience|33|True|Clap
Audience|34|True|Clap
Audience|35|True|Clap
Audience|36|True|Clap
Audience|37|True|Clap
Audience|38|True|Clap
Audience|39|True|Clap
Audience|40|False|Clap
Mayor of Komona|42|False|Aquesta poció pot tornar rica a tota Komona!
Mayor of Komona|41|True|Fantàstic! Increïble!
Audience|44|True|Clap
Audience|45|True|Clap
Audience|46|True|Clap
Audience|47|True|Clap
Audience|48|True|Clap
Audience|49|True|Clap
Audience|50|True|Clap
Audience|51|True|Clap
Audience|52|True|Clap
Audience|53|True|Clap
Audience|54|True|Clap
Audience|55|True|Clap
Audience|56|True|Clap
Audience|57|True|Clap
Audience|58|True|Clap
Audience|59|True|Clap
Audience|60|False|Clap
Mayor of Komona|2|False|La Coriander desafia a la pròpia Mort amb aquesta poció mi-ra-cu-lo-sa!
Saffron|21|True|La veritable poció que han estat esperant: la que sorprendrà als seus amics i veïns...
Mayor of Komona|43|False|Els seus aplaudiments no es poden equivocar. La Coriander ja ha sigut eliminada!
Saffron|20|False|poció.

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|Aquesta demostració serà difícil de ser superada per la... Shichimi!
Shichimi|4|True|NO !
Shichimi|5|True|No puc, és massa perillosa!
Shichimi|6|False|HO SENTO!
Mayor of Komona|3|False|...au va Shichimi, tothom t'està esperant!
Mayor of Komona|7|False|Sembla ser, dames i cavallers, que la Shichimi es retira!
Saffron|8|False|Dona'm això!
Saffron|9|False|...i para de fer-te la tímida. Estàs enviant tota la diversió a n'orris...
Saffron|10|False|...tothom sap ja que he guanyat el torneig. Tant se val el que faci la teva poció...
Shichimi|11|False|!!!
Sound|12|False|WAAARK!!!|nowhitespace
Shichimi|15|False|MONSTRES GEGANTS!
Shichimi|2|False|Jo... No sabia que hauríem de fer-ne una demostració...
Shichimi|13|True|COMPTE!!!
Shichimi|14|True|És una poció de

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bird|1|False|CAW-CAW-C aa aa aa aa w w ww|nowhitespace
Sound|2|False|BAM!
Pepper|3|True|Quina canya!
Pepper|5|False|...la meva poció com a mínim servirà per arrencar-vos unes rialles perque...
Pepper|4|False|què em toca ja?
Mayor of Komona|6|True|Fuig, idiota!
Mayor of Komona|7|False|El torneig s'ha acabat! Salva la pell!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|2|False|...com sempre, tothom marxa quan arriba el nostre torn.
Pepper|1|True|ja ho tenim...
Pepper|4|True|Com a mínim tinc una idea de què en podrem fer, de la nostra “poció”, Carrot...
Pepper|5|False|...posar tot en odre aquí i tornar cap a casa!
Pepper|7|True|Tu, tros de
Pepper|8|False|Canari-zombi-empolainat-gegant!
Pepper|10|False|Vols provar una última poció?...
Pepper|11|False|...no massa, eh?
Pepper|6|False|EIII!
Sound|9|False|C R AC K !|nowhitespace

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Exacte, llegeix-ne l'etiqueta atentament...
Pepper|2|False|...no m'ho pensaré pas dos cops abans d'abocar-te-la per sobre si no toques el dos de Komona ara mateix!
Mayor of Komona|3|True|Per haver salvat la nostra vila quan estava en greu perill,
Mayor of Komona|4|False|atorguem el primer premi a la Pepper i a la seva poció de...?
Pepper|7|False|...huh... de fet, no és realment una poció; és una mostra del pipí del meu gat, de la seva visita al veterinari!
Pepper|6|True|...haha! si...
Pepper|8|False|...així doncs, en cal cap demostració?...
Narrator|9|False|Episodi 6 : El torneig de pocions
Narrator|10|False|FI
Writing|5|False|50.000 Kos
Credits|11|False|Març 2015 - Dibuix i història per David Revoy - Traducció al català per Juan José Segura

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Pepper&Carrot és completament gratuït, open-source i finançat gràcies al mecenatge dels lectors. Donem les gràcies per aquest episodi a 245 mecenes:
Credits|4|False|https://www.patreon.com/davidrevoy
Credits|3|False|Tu també pots esdevenir un mecenes de Pepper&Carrot pel proper capítol:
Credits|7|False|Eines: Aquest capítol ha estat fet amb eines 100% Free/Libre software Krita en Linux Mint
Credits|6|False|Open-source : all source files amb les capes i fitxers estan disponibles.
Credits|5|False|Llicència : Creative Commons Attribution Pots compartir-lo, reenviar-ho...
Credits|2|False|Алексей ★ Глеб Бузало ★ 獨孤欣 & 獨弧悦 ★ Addison Lewis ★ A Distinguished Robot ★ Adrian Lord ★ Ahmad Ali ★ Aina Reich ★ Alandran ★ Alan Hardman ★ Albert Westra ★ Alcide ★ Alejandro Flores Prieto ★ Alex ★ Alexander Bülow Tomassen ★ Alexander Sopicki ★ Alexandra Jordan ★ Alex Lusco ★ Alex Silver ★ Alex Vandiver ★ Alfredo ★ Ali Poulton (Aunty Pol) ★ Allan Zieser ★ Andreas Rieger ★ Andrej Kwadrin ★ Andrew ★ Andrew Godfrey ★ Andrey Alekseenko ★ Angela K ★ Anna Orlova ★ Antan Karmola ★ Anthony Edlin ★ Antonio Mendoza ★ Ardash Crowfoot ★ Arjun Chennu ★ Arne Brix ★ Aslak Kjølås-Sæverud ★ Axel Bordelon ★ Axel Philipsenburg ★ Barbix ★ Ben Evans ★ Bernd ★ Betsy Luntao ★ Boonsak Watanavisit ★ Boris Fauret ★ Boudewijn Rempt ★ BoxBoy ★ Brett Smith ★ Brian Behnke ★ Brian Smith ★ Bryan Butler ★ Bui Dang Hai Trieu ★ Carlos Levischi ★ Charlotte Lacombe-bar ★ Chris Radcliff ★ Chris Sakkas ★ Christian Gruenwaldner ★ Christophe Carré ★ Christopher Bates ★ Clara Dexter ★ codl ★ Colby Driedger ★ Conway Scott Smith ★ Cuthbert Williams ★ Cyrille Largillier ★ Cyril Paciullo ★ Damien ★ Daniel ★ Daniel Björkman ★ Danny Grimm ★ David Tang ★ DiCola Jamn ★ Dmitry ★ Donald Hayward ★ Duke ★ Eitan Goldshtrom ★ Enrico Billich ★ Epsilon ★ Eric Schulz ★ Faolan Grady ★ Francois Schnell ★ Garret Patterson ★ Ginny Hendricks ★ GreenAngel5 ★ Grigory Petrov ★ Guillaume ★ Guillaume Ballue ★ Gustav Strömbom ★ Guy Davis ★ Happy Mimic ★ Helmar Suschka ★ Henning Döscher ★ Ilyas ★ Irina Rempt ★ Ivan Korotkov ★ James Frazier ★ Janusz ★ Jared Tritsch ★ JDB ★ Jean-Baptiste Hebbrecht ★ Jean-Gabriel LOQUET ★ Jeffrey Schneider ★ Jessey Wright ★ Jim ★ Jim Street ★ Jiska ★ Joachim Schiele ★ JoÃ£o Luiz Machado Junior ★ Joern Konopka ★ joe rutledge ★ Johanne Thomson ★ John ★ John Urquhart Ferguson ★ Jónatan Nilsson ★ Jonathan Leroy ★ Jonathan Ringstad ★ Jon Brake ★ Jorge Bernal ★ Joseph Bowman ★ Julien Duroure ★ Justus Kat ★ Kai-Ting (Danil) Ko ★ Kasper Hansen ★ Kate ★ Kathryn Wuerstl ★ Ken Mingyuan Xia ★ Kingsquee ★ Kroet ★ Levi Kornelsen ★ Liselle ★ Lorentz Grip ★ L S ★ Luc Stepniewski ★ Luke Hochrein ★ MacCoy ★ Magnus Kronnäs ★ Manuel ★ Marc & Rick ★ Marcus ★ Martin Owens ★ Mary Brownlee ★ Masked Admirer ★ Mathias Stærk ★ Mefflin Ross Bullis-bates ★ Michael ★ Michael Gill ★ Michael Pureka ★ Michelle Pereira Garcia ★ Mike Mosher ★ Miroslav ★ Muzyka Dmytro ★ Nataya Castillo ★ Nazhif ★ Nicholas DeLateur ★ Nicholas Terranova ★ Nicki Aya ★ Nicola Angel ★ Nicolae Berbece ★ Nicole Heersema ★ Nielas Sinclair ★ NinjaKnight Comics ★ Noble Hays ★ Noelia Calles Marcos ★ Nora Czaykowski ★ Nyx ★ Olivier Amrein ★ Olivier Brun ★ Omar Willey ★ Oscar Moreno ★ Öykü Su Gürler ★ Ozone S. ★ Pablo Lopez Soriano ★ Pat David ★ Patrick Gamblin ★ Paul ★ Pavel Semenov ★ Pet0r ★ Peter ★ Peter Moonen ★ Petr Vlašic ★ Philippe Jean Edward Bateman ★ Pierre Geier ★ Pierre Vuillemin ★ Pranab Shenoy ★ Pyves & Ran ★ Raghavendra Kamath ★ Rajul Gupta ★ Reorx Meng ★ Ret Samys ★ Rictic ★ RJ van der Weide ★ Roberto Zaghis ★ Roman ★ Rumiko Hoshino ★ Rustin Simons ★ Sally Bridgewater ★ Sami T ★ Samuel Mitson ★ Scott Petrovic ★ Sean Adams ★ Shadefalcon ★ ShadowMist ★ Shafak ★ Shawn Meyer ★ Simon Forster ★ Simon Isenberg ★ Sonny W. ★ Soriac ★ Stanislav Vodetskyi ★ Stephanie Cheshire ★ Stephen Bates ★ Stephen Smoogen ★ Steven Bennett ★ Stuart Dickson ★ Surt ★ TamaskanLEM ★ Tar8156 ★ Terry Hancock ★ TheFaico ★ Thomas Citharel ★ Thomas Courbon ★ Thomas Schwery ★ Tim Burbank ★ Tim J. ★ Tomas Hajek ★ Tom Demian ★ Tom Savage ★ Tracey Reuben ★ Travis Humble ★ Tree ★ Tyson Tan ★ Urm ★ Victoria ★ Victoria White ★ Vladislav Kurdyukov ★ Vlad Tomash ★ Westen Curry ★ Witt N. Vest ★ Xavier Claude ★ Yalyn Vinkindo ★ Yaroslav ★ Zeni Pong ★ Źmicier Kušnaroŭ
