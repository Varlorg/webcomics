# Transcript of Pepper&Carrot Episode 29 [da]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 29: Verdens-ødelæggeren

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Monster|2|True|…Endelig!
Monster|3|False|En interdimensionel revne!
Monster|4|False|Sikkert på grund af en kosmisk hændelse eller en vigtig begivenhed!
Monster|5|True|Voks… Voks, lille revne !
Monster|6|False|Og vis mig denne nye verden jeg kan TRÆLBINDE og DOMINERE !
Monster|7|True|Aha…
Monster|8|False|Det er interessant.
Monster|9|False|Man kan endda sige, at det er min lykkedag...
Fortæller|1|False|Samtidig, i en anden dimension…

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Monster|1|False|En dimension beboet af intelligente skabninger!
Monster|2|False|Voks, lille revne !
Monster|3|False|Voks… Muhaha hahaha !
Pepper|4|True|Pyha!
Pepper|5|False|Sikke en aften, venner!
Koriander|6|False|Pepper, jeg har lige talt med Safran om, at vi ikke har en anelse om, hvordan din Kaosah-magi fungerer.
Safran|7|False|Det er lidt af et mysterium for os. Kan du fortælle mere om det?

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Haha, det bliver lidt kompliceret!
Pepper|2|True|Men okay, man kan sige, at det er baseret på forståelsen af lovene bag alle kaotiske systemer…
Pepper|3|False|…fra de mindste til de største.
Koriander|4|False|Ja, det gør det jo meget mere klart...
Pepper|7|False|Arg! Vent lidt, jeg giver jer et eksempel.
Pepper|8|False|Giv mig lige to minutter og lidt Kaosah-fornuft…
Pepper|9|False|Fundet!
Lyd|5|True|KRADSE
Lyd|6|False|KRADSE
Pepper|10|True|Se denne tandstik, der sad fast mellem to brosten.
Pepper|11|False|Nu hvor jeg har fundet den og samlet den op, forhindrer det helt sikkert at nogen træder på den.
Pepper|12|True|En lille positiv ændring, som kan have enorme konsekvenser i livets store kaotiske system.
Pepper|13|False|Dét er Kaosah!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|False|Føj!
Koriander|2|False|Bvadr! Den der ting har været i nogens mund!
Safran|3|False|Haha! Imponerende som altid, Pepper !
Koriander|4|False|Nåh, tak Pepper for denne… "forklaring".
Koriander|5|True|Det er tid til at sove, tror I ikke?
Koriander|6|False|Og vaske hænder for nogens vedkommende.
Pepper|7|False|Hey! Vent lige på mig!
Lyd|8|False|Pok!
Lyd|9|True|Boink!
Lyd|10|False|Boink!
Lyd|11|False|S LAM !|nowhitespace
Carrot|12|False|?

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|1|False|Ssss!
Lyd|2|False|Dunk!
Lyd|3|False|Tok!
Lyd|4|False|K LIIR !|nowhitespace
Lyd|5|False|Fr rfrf…|nowhitespace

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|We have a patch for the artwork in case your text is too short or is not easily split in two words. Ask David or Midgard for more information. Look at the Korean version for an example.
<hidden>|0|False|NOTE FOR TRANSLATORS
Monster|7|False|TIL ANGREB!!
Monster|6|True|Mouhaha HA HA ! Endelig !
Monster|9|False|!?
Skrift|1|True|LAGER
Skrift|2|False|FYRVÆRKERI
Lyd|3|False|Fr rUuf ! !|nowhitespace
Lyd|4|False|Fr r ! !|nowhitespace
Lyd|5|False|BANG!
Lyd|8|False|Swiff ! !|nowhitespace

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|3|False|B UM !|nowhitespace
Lyd|2|False|B AN G!|nowhitespace
Lyd|1|False|P A F !|nowhitespace
Lyd|4|False|Swiff !|nowhitespace
Lyd|5|False|PU F !|nowhitespace
Pepper|6|True|Bare rolig, Carrot.
Pepper|7|False|Det er nok bare nogen, der stadigvæk fester…
Pepper|8|False|Kom nu, i seng!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Monster|5|False|...
Spidskommen|1|False|Virkelig? …Kunne hun have udløst sådan en enorm kædereaktion uden overhovet at lægge mærke til det?
Cayenne|2|False|Uden tvivl.
Timian|3|False|Mine damer, jeg tror at vores Pepper endelig er klar!
Lyd|6|False|Plop!
Lyd|4|False|Psh hh ! !|nowhitespace
Fortæller|7|False|- TRILOGIEN OM KORIANDERS KRONING, SLUT -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|5|True|Du kan også blive tilhænger og få dit navn skrevet her!
Pepper|3|True|Pepper & Carrot er fri, open-source og sponsoreret af sine læsere.
Pepper|4|False|Denne episode blev støttet af 960 tilhængere!
Pepper|7|True|Gå på www.peppercarrot.com og få mere information!
Pepper|6|True|Vi er på Patreon, Tipeee, PayPal, Liberapay … og andre!
Pepper|8|False|Tak!
Pepper|2|True|Vidste I det?
Credits|1|False|25. April 2019 Tegning og manuskript: David Revoy. Genlæsning i beta-versionen: CalimeroTeknik, Craig Maloney, Martin Disch, Midgard, Nicolas Artance, Valvin. Dansk Version Oversættelse: Emmiline Alapetite Rettelser: Rikke & Alexandre Alapetite Baseret på Hereva-universet Skabt af: David Revoy. Vedligeholdt af: Craig Maloney. Medforfattere: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Rettelser: Willem Sonke, Moini, Hali, CGand, Alex Gryson . Værktøj: Krita 4.1.5~appimage, Inkscape 0.92.3 sur Kubuntu 18.04.1. Licens: Creative Commons Attribution 4.0. www.peppercarrot.com
<hidden>|0|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|0|False|You can also translate this page if you want.
<hidden>|0|False|Beta readers help with the story, proofreaders give feedback about the text.
