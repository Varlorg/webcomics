# Transcript of Pepper&Carrot Episode 02 [sl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Epizoda 2: Mavrični napoji
<hidden>|2|False|Korenček

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|5|True|Glu
Sound|6|True|Glu
Sound|7|False|Glu
Writing|1|True|POZOR
Writing|3|False|ČAROVNICE
Writing|2|True|LASTNINA
Writing|4|False|33

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|7|False|tok|nowhitespace
Writing|1|False|FIRE D
Writing|2|False|DEEP OCEAN
Writing|3|False|VIOLE(N)T
Writing|4|False|ULTRA BLUE
Writing|5|False|PIN
Writing|6|False|MSON
Writing|8|False|NATURE
Writing|9|False|YELLOW
Writing|10|False|ORANGE TOP
Writing|11|False|FIRE DANCE
Writing|12|False|DEEP OCEAN
Writing|13|False|VIOLE(N)T
Writing|14|False|ULTRA BLUE
Writing|15|False|META PINK
Writing|16|False|MAGENTA X
Writing|17|False|CRIMSON

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|Glu
Sound|2|True|Glu
Sound|3|False|Glu

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|Glu
Sound|2|False|Glu
Sound|3|True|Glu
Sound|4|False|Glu
Sound|5|True|Glu
Sound|6|False|Glu
Sound|20|False|m|nowhitespace
Sound|19|True|m|nowhitespace
Sound|18|True|M|nowhitespace
Sound|7|True|P|nowhitespace
Sound|8|True|f|nowhitespace
Sound|9|True|u|nowhitespace
Sound|10|False|j !|nowhitespace
Sound|11|True|T|nowhitespace
Sound|12|True|f|nowhitespace
Sound|13|True|u|nowhitespace
Sound|14|True|u|nowhitespace
Sound|15|True|u|nowhitespace
Sound|16|True|u|nowhitespace
Sound|17|False|j|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|B
Sound|2|True|lup|nowhitespace
Sound|3|True|B
Sound|4|False|lup|nowhitespace
Sound|7|True|ap|nowhitespace
Sound|6|True|l|nowhitespace
Sound|5|True|č
Sound|10|True|ap|nowhitespace
Sound|9|True|l|nowhitespace
Sound|8|True|č
Sound|13|False|ap|nowhitespace
Sound|12|True|l|nowhitespace
Sound|11|True|č

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|True|Ta web-strip je odprtokoden in to epizodo je financiralo 21 sponzorjev na
Credits|2|False|www.patreon.com/davidrevoy
Credits|3|False|Zahvaljujem se
Credits|4|False|ustvarjeno s Krita na GNU/Linux
