# Transcript of Pepper&Carrot Episode 02 [kw]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Rann 2: Ismegennow Kammneves

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|5|True|Glupp
Sound|6|True|Glupp
Sound|7|False|Glupp
Writing|4|False|33
Writing|1|True|GWARNYANS
Writing|2|True|ANNEDH
Writing|3|False|GWRAGH

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|7|False|tokk|nowhitespace
Writing|1|False|DONS
Writing|2|False|MOR DOWN
Writing|3|False|PURPURA
Writing|4|False|GORLAS
Writing|5|False|DHW
Writing|6|False|KOGH
Writing|8|False|NATUR
Writing|9|False|MELYN
Writing|10|False|PENN OWR
Writing|11|False|DONS TAN
Writing|12|False|MOR DOWN
Writing|13|False|PURPURA
Writing|14|False|GORLAS
Writing|15|False|GWYNNRUDH
Writing|16|False|RUDHWYNN
Writing|17|False|KOGH

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|Glupp
Sound|2|True|Glupp
Sound|3|False|Glupp

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|20|False|m|nowhitespace
Sound|19|True|m|nowhitespace
Sound|18|True|M
Sound|7|True|T
Sound|8|True|r|nowhitespace
Sound|9|True|e|nowhitespace
Sound|10|False|wa!|nowhitespace
Sound|11|True|H
Sound|12|True|w|nowhitespace
Sound|13|True|y|nowhitespace
Sound|14|True|j|nowhitespace
Sound|15|True|a|nowhitespace
Sound|16|True|a|nowhitespace
Sound|17|False|!|nowhitespace
Sound|1|True|Klunk
Sound|2|False|Klunk
Sound|3|True|Klunk
Sound|4|False|Klunk
Sound|5|True|Klunk
Sound|6|False|Klunk

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|B
Sound|2|True|lupp|nowhitespace
Sound|3|True|B
Sound|4|False|lupp|nowhitespace
Sound|7|True|upp|nowhitespace
Sound|6|True|pl|nowhitespace
Sound|5|True|s
Sound|10|True|upp|nowhitespace
Sound|9|True|pl|nowhitespace
Sound|8|True|s
Sound|13|False|upp|nowhitespace
Sound|12|True|pl|nowhitespace
Sound|11|True|s

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|True|Fenten ygor yw an roskomik ma hag arghesys veu an dyllans ma gans ow 21 tasek
Credits|2|False|www.patreon.com/davidrevoy
Credits|3|False|Meur ras bras dhe:
Credits|4|False|gwrys gans Krita war GNU/Linux
