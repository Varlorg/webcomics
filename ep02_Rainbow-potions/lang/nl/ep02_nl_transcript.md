# Transcript of Pepper&Carrot Episode 02 [nl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Aflevering 2: Regenboogdrankjes

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|5|True|Gloep
Sound|6|True|Gloep
Sound|7|False|Gloep
Writing|1|True|WAARSCHUWING
Writing|3|False|PRIVÉ-EIGENDOM
Writing|2|True|HEKS
Writing|4|False|33

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|7|False|tok|nowhitespace
Writing|1|False|FIRE D
Writing|2|False|DEEP OCEAN
Writing|3|False|VIOLE(N)T
Writing|4|False|ULTRA BLUE
Writing|5|False|PIN
Writing|6|False|MSON
Writing|8|False|NATURE
Writing|9|False|YELLOW
Writing|10|False|ORANGE TOP
Writing|11|False|FIRE DANCE
Writing|12|False|DEEP OCEAN
Writing|13|False|VIOLE(N)T
Writing|14|False|ULTRA BLUE
Writing|15|False|META PINK
Writing|16|False|MAGENTA X
Writing|17|False|CRIMSON

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|Gloep
Sound|2|True|Gloep
Sound|3|False|Gloep

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|Gulp
Sound|2|False|Gulp
Sound|3|True|Gulp
Sound|4|False|Gulp
Sound|5|True|Gulp
Sound|6|False|Gulp
Sound|21|False|...!|nowhitespace
Sound|20|True|egh|nowhitespace
Sound|19|True|w|nowhitespace
Sound|18|True|B
Sound|7|True|S
Sound|8|True|P|nowhitespace
Sound|9|True|l|nowhitespace
Sound|10|False|urp!|nowhitespace
Sound|11|True|S
Sound|12|True|S|nowhitespace
Sound|13|True|S|nowhitespace
Sound|14|True|P|nowhitespace
Sound|15|True|l|nowhitespace
Sound|16|True|a|nowhitespace
Sound|17|False|t|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|B
Sound|2|True|lub|nowhitespace
Sound|3|True|B
Sound|4|False|lub|nowhitespace
Sound|7|True|ut|nowhitespace
Sound|6|True|pl|nowhitespace
Sound|5|True|s
Sound|10|True|ut|nowhitespace
Sound|9|True|pl|nowhitespace
Sound|8|True|s
Sound|13|False|ut|nowhitespace
Sound|12|True|pl|nowhitespace
Sound|11|True|s

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|True|Deze webcomic is open-bron, en deze aflevering is gesposord door mijn 21 patrons op
Credits|2|False|www.patreon.com/davidrevoy
Credits|3|False|Veel dank aan
Credits|4|False|gemaakt met Krita op GNU/Linux
