# Transcript of Pepper&Carrot Episode 02 [vi]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Tập 2: Thuốc cầu vồng

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|5|True|Ục
Sound|6|True|Ục
Sound|7|False|ục
Writing|1|True|WARNING
Writing|3|False|PROPERTY
Writing|2|True|WITCH
Writing|4|False|33

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|7|False|coong|nowhitespace
Writing|1|False|FIRE D
Writing|2|False|DEEP OCEAN
Writing|3|False|VIOLE(N)T
Writing|4|False|ULTRA BLUE
Writing|5|False|PIN
Writing|6|False|MSON
Writing|8|False|NATURE
Writing|9|False|YELLOW
Writing|10|False|ORANGE TOP
Writing|11|False|FIRE DANCE
Writing|12|False|DEEP OCEAN
Writing|13|False|VIOLE(N)T
Writing|14|False|ULTRA BLUE
Writing|15|False|META PINK
Writing|16|False|MAGENTA X
Writing|17|False|CRIMSON

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|ỤC
Sound|2|True|Ục
Sound|3|False|Ục

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|Ực
Sound|2|False|Ực
Sound|3|True|Ực
Sound|4|False|Ực
Sound|5|True|Ực
Sound|6|False|Ực
Sound|20|False|m|nowhitespace
Sound|19|True|m|nowhitespace
Sound|18|True|M
Sound|7|True|P|nowhitespace
Sound|8|True|h|nowhitespace
Sound|9|False|ụt !|nowhitespace
Sound|10|True|S
Sound|11|True|S|nowhitespace
Sound|12|True|S|nowhitespace
Sound|13|True|P|nowhitespace
Sound|14|True|l|nowhitespace
Sound|15|True|a|nowhitespace
Sound|16|False|t|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|B
Sound|2|True|lup|nowhitespace
Sound|3|True|B
Sound|4|False|lup|nowhitespace
Sound|7|True|up|nowhitespace
Sound|6|True|pl|nowhitespace
Sound|5|True|s
Sound|10|True|up|nowhitespace
Sound|9|True|pl|nowhitespace
Sound|8|True|s
Sound|13|False|up|nowhitespace
Sound|12|True|pl|nowhitespace
Sound|11|True|s

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|True|Đây là truyện tranh miễn phí và tập truyện này được tài trợ bởi 21 người qua
Credits|2|False|www.patreon.com/davidrevoy
Credits|3|False|Lời cảm ơn tới:
Credits|5|False|made with Krita on GNU/Linux
Credits|4|False|Dịch bởi: Binh Pham
