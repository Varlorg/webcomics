# Transcript of Pepper&Carrot Episode 22 [da]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 22: Afstemningssystemet

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Komonas Borgmester|1|False|Nu kan vores store tryllekonkurrence endelig begynde!
Skrift|2|False|Tryllekonkurrence

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Komonas Borgmester|1|False|Og takket være vores strålende ingeniører kan I også deltage!
Komonas Borgmester|2|False|Mine kære venner! Se disse små teknologiske vidundere, som vores værtinder vil dele ud!
Komonas Borgmester|3|False|Den grønne knap giver et point til en deltager og den røde knap fjerner et point: det er jer der vælger!
Komonas Borgmester|4|True|“Og hvad så med juryen?” spørger I mig.
Komonas Borgmester|5|False|Bare rolig, vi har tænkt på alt!
Komonas Borgmester|6|False|De får alle en speciel boks, som kan give eller fjerne hundrede point på én gang!
Pepper|7|False|Wow!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Komonas Borgmester|5|False|…50 000 Ko !
Komonas Borgmester|1|False|Og prikken over i’et er, at resultaterne vil vises lige over konkurrenterne!
Komonas Borgmester|3|False|De tre hekse, som får flest point, fortsætter i finalen!
Komonas Borgmester|4|True|Finalen, hvor vinderen får den nette sum af…
Skrift|2|False|1337
<hidden>|0|False|Edit this one, all others are linked
Publikum|6|False|Klap

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Carrot|2|False|Klik
Pepper|3|False|Et fantastisk koncept! Ik' også, Carrot?
Pepper|4|True|Innovativt…
Pepper|5|True|Sjovt…
Pepper|6|True|Demokratisk…
Pepper|7|False|…det perfekte system!
Pepper|8|True|Der er ikke længere brug for eksperter til at bedømme kvaliteten!
Pepper|9|False|Sikke et fremskridt! Vi lever virkelig i en fantastisk tid!
Komonas Borgmester|10|False|Har I alle fået én?
Publikum|11|False|Super!
Publikum|12|False|Ja!
Publikum|13|False|Ja!
Publikum|14|False|Ja!
Publikum|15|False|Ja!
Komonas Borgmester|16|True|Godt!
Komonas Borgmester|17|True|Lad konkurrencen…
Komonas Borgmester|18|False|BEGYNDE!!
<hidden>|0|False|Edit this one, all others are linked
Publikum|1|False|Klap

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Komonas Borgmester|1|False|Og det er Kamille, der skal åbne ballet!
Lyd|2|False|Djiiiuu…
Kamille|3|False|SYLVESTRIS!
Lyd|4|False|Paf !
Publikum|5|True|Klik
Publikum|6|True|Klik
Publikum|7|True|Klik
Publikum|8|True|Klik
Publikum|9|True|Klik
Publikum|10|True|Klik
Publikum|11|False|Klik

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Komonas Borgmester|2|False|Kamille fik et flot resultat! Nu er det Shichimis tur!
Skrift|1|False|5861
Shichimi|4|False|LUX…
Lyd|5|False|Fii iiiii zz !!|nowhitespace
Shichimi|6|False|MAXIMA!
Publikum|7|False|Aah!!
Publikum|8|False|Hiii!!
Publikum|9|False|Mine øjne!!
Pepper|12|False|Carrot… Giv hende alligevel en grøn tommel, hun er vores ven…
Carrot|13|False|Tap
Publikum|10|True|Uuh!
Publikum|11|False|Buh !
Publikum|14|True|Uu h!
Publikum|15|False|Buh!
Komonas Borgmester|17|False|Nå, det ser ud til, at publikum ikke var så vilde med denne “strålende” opvisning! Nu er det Spirulina!
Skrift|16|False|-42
Publikum|18|True|Uuh!
Publikum|19|True|Buh!
Publikum|20|True|Buh!
Publikum|21|False|Uuh!
<hidden>|0|False|Edit this one, all others are linked
Publikum|3|False|Klap

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Spirulina|1|False|RELEASUS KRAKENIS!
Lyd|2|False|Www looo !|nowhitespace
Lyd|3|False|S plaa shh !|nowhitespace
Komonas Borgmester|5|False|Flot! Kraftfuldt! Spirulina fører! Koriander, nu er det Dem!
Skrift|4|False|6225
Spirulina & Durian|6|False|Klask
Koriander|8|False|MORTUS REDITUS!
Lyd|9|False|Groo wooo !|nowhitespace
Komonas Borgmester|11|False|Nå, de ser ud til, at skeletter er gået af mode… Nu til vores kære Safran!
Skrift|10|False|2023
<hidden>|0|False|Edit this one, all others are linked
Publikum|7|False|Klap

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Safran|1|False|Grrr! Ikke tale om at gøre det dårligere end Spirulina! Nu skal jeg gøre mit bedste!
Safran|2|False|Flyt dig Trøffel!
Lyd|3|False|Fr rrshh !|nowhitespace
Lyd|4|False|Fr rrshh !|nowhitespace
Lyd|5|False|Krchh!
Trøffel|6|False|Miv!
Safran|7|False|SPIRALIS
Safran|8|False|FLAMA aaaa aaah!|nowhitespace
Lyd|9|False|Fr rrooo oshh !|nowhitespace
Lyd|10|False|Swwwiiip!
Lyd|11|False|Fr rrh !|nowhitespace
Lyd|12|False|Bum!
Safran|13|False|?!!
Lyd|14|False|Fr rrh !|nowhitespace
Lyd|15|False|Fr rrh !|nowhitespace
Safran|16|False|Ahhhhh! !|nowhitespace

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Safran|1|False|Åh nej… Så pinligt!
Safran|3|False|?!
Skrift|4|True|14
Skrift|5|False|849
Pepper|6|False|Klik
Lord Azeirf|7|True|Klik
Lord Azeirf|8|True|Klik
Lord Azeirf|9|False|Klik
Skrift|10|True|18
Skrift|11|False|231
Komonas Borgmester|12|True|Undskyld, Undskyld! Lidt anstændighed, tak!
Komonas Borgmester|13|False|Shichimi og Koriander er ude!
Komonas Borgmester|14|False|Kamille, Spirulina og Safran går videre i finalen!
<hidden>|0|False|Edit this one, all others are linked
Publikum|2|False|Klap

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Komonas Borgmester|1|False|Mine frøkner, er I klar til denne sidste runde?
Komonas Borgmester|2|False|Værsgo!
Skrift|3|True|1 5|nowhitespace
Skrift|4|False|703
Skrift|5|True|19
Skrift|6|False|863
Skrift|7|True|1 3|nowhitespace
Skrift|8|False|614
Lyd|10|False|Bum!
Pepper|11|False|Jeg trækker alt, jeg har sagt om systemet tilbage…
Lord Azeirf|12|True|Klik
Lord Azeirf|13|True|Klik
Lord Azeirf|14|False|Klik
Fortæller|15|False|- SLUT -
<hidden>|0|False|Edit this one, all others are linked
Publikum|9|False|Klap

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|05/2017 – www.peppercarrot.com – Tegning og manuskript: David Revoy – Dansk oversættelse: Emmiline Alapetite
Credits|2|False|Genlæsning og hjælp til dialogerne: Nicolas Artance, Valvin
Credits|3|False|Baseret på Hereva-universet skabt af David Revoy med bidrag af Craig Maloney. Rettelser af Willem Sonke, Moini, Hali, CGand et Alex Gryson.
Credits|4|False|Værktøj: Krita 3.1.3, Inkscape 0.92.1 på Linux Mint 18.1
Credits|5|False|Licens: Creative Commons Attribution 4.0
Credits|6|False|Pepper&Carrot er helt gratis, open-source, og sponsoreret af læsere. Til denne episode, tak til de 864 tilhængere:
Credits|7|False|Støt næste episode af Pepper&Carrot på www.patreon.com/davidrevoy
