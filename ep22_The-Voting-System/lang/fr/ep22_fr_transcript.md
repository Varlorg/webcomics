# Transcript of Pepper&Carrot Episode 22 [fr]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titre|1|False|Épisode 22 : Le système de vote

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Maire de Komona|1|False|Notre grand concours de magie peut enfin commencer !
Écriture|2|False|Concours de Magie

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Maire de Komona|1|False|Et grâce à nos brillants ingénieurs, vous allez pouvoir participer vous aussi !
Maire de Komona|2|False|En effet mes chers amis, admirez ces petites merveilles technologiques que nos hôtesses vont vous distribuer !
Maire de Komona|3|False|Le bouton vert donne un point à une candidate, le bouton rouge en enlève un : à vous de choisir !
Maire de Komona|4|True|« Et le jury, alors ? », me demanderez-vous.
Maire de Komona|5|False|Pas de panique, nous avons pensé à tout !
Maire de Komona|6|False|Chacun d'entre eux recevra un boîtier spécial, capable de donner ou retirer cent points d'un seul coup !
Pepper|7|False|Waouh !

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Maire de Komona|5|False|…50 000 Ko !
Maire de Komona|1|False|Et cerise sur le gâteau, les scores apparaîtront directement au-dessus des participantes !
Maire de Komona|3|False|Les trois sorcières qui obtiennent le meilleur score accèdent à la finale !
Maire de Komona|4|True|Finale au terme de laquelle la grande gagnante remportera la coquette somme de…
Écriture|2|False|1337
<hidden>|0|False|Edit this one, all others are linked
Audience|6|False|Clap

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Carrot|2|False|Tap
Pepper|3|False|Sympa comme concept ! N'est-ce pas, Carrot ?
Pepper|4|True|Innovant…
Pepper|5|True|Amusant…
Pepper|6|True|Démocratique…
Pepper|7|False|…le système parfait, quoi !
Pepper|8|True|Plus besoin d'experts pour juger la qualité !
Pepper|9|False|Quel progrès ! On vit vraiment une époque fantastique !
Maire de Komona|10|False|Tout le monde a son boîtier ?
Audience|11|False|Super !
Audience|12|False|Oui !
Audience|13|False|Oui !
Audience|14|False|Oui !
Audience|15|False|Oui !
Maire de Komona|16|True|Bien !
Maire de Komona|17|True|Que le concours…
Maire de Komona|18|False|COMMENCE !!
<hidden>|0|False|Edit this one, all others are linked
Audience|1|False|Clap

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Maire de Komona|1|False|Et c'est à Camomille d'ouvrir le bal !
Son|2|False|Dgiiioo…
Camomille|3|False|SYLVESTRIS !
Son|4|False|Bam !
Audience|5|True|Tap
Audience|6|True|Tap
Audience|7|True|Tap
Audience|8|True|Tap
Audience|9|True|Tap
Audience|10|True|Tap
Audience|11|False|Tap

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Maire de Komona|2|False|Camomille obtient un très bon score ! C'est à présent au tour de Shichimi !
Écriture|1|False|5861
Shichimi|4|False|LUX…
Son|6|False|Fii iiiii zz !!|nowhitespace
Shichimi|5|False|MAXIMA !
Audience|7|False|Aah !!
Audience|8|False|Hiii !!
Audience|9|False|Mes yeux !!!
Pepper|12|False|Carrot… Mets-lui quand même un pouce vert, c'est notre amie…
Carrot|13|False|Tap
Audience|10|True|Hoûû !
Audience|11|False|Bouuh !
Audience|14|True|Hoûû !
Audience|15|False|Bouh !
Maire de Komona|17|False|Ah, on dirait que le public n'a pas été séduit par cette « brillante » prestation ! C'est au tour de Spiruline !
Écriture|16|False|-42
Audience|18|True|Hoûû !
Audience|19|True|Bouh !
Audience|20|True|Hoûû !
Audience|21|False|Hoû !
<hidden>|0|False|Edit this one, all others are linked
Audience|3|False|Clap

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Spiruline|1|False|RELEASUS KRAKENIS !
Son|2|False|Www loo !|nowhitespace
Son|3|False|S plaa shh !|nowhitespace
Maire de Komona|5|False|Joli ! Puissant ! Spiruline mène à présent au score ! Coriandre, c'est à vous !
Écriture|4|False|6225
Spiruline et Durian|6|False|Tap
Coriandre|8|False|MORTUS REDITUS !
Son|9|False|Groo wooo !|nowhitespace
Maire de Komona|11|False|Ah, il semblerait que les squelettes soient démodés… À notre chère Safran !
Écriture|10|False|2023
<hidden>|0|False|Edit this one, all others are linked
Audience|7|False|Clap

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Safran|1|False|Grrr ! Pas question de faire moins bien que cette Spiruline ! C'est le moment de donner le maximum !
Safran|2|False|Écarte-toi Truffel !
Son|3|False|Fr rrshh !|nowhitespace
Son|4|False|Fr rrshh !|nowhitespace
Son|5|False|Krchh !
Truffel|6|False|Mia !
Safran|7|False|SPIRALIS
Safran|8|False|FLAMA aaaa aaah !|nowhitespace
Son|9|False|Fr rrooo oshh !|nowhitespace
Son|10|False|Swwwiiip !
Son|11|False|Fr rrh !|nowhitespace
Son|12|False|Paf !
Safran|13|False|?!!
Son|14|False|Fr rrh !|nowhitespace
Son|15|False|Fr rrh !|nowhitespace
Safran|16|False|Hiiiiiiiii ! !|nowhitespace

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Safran|1|False|Oh non… La honte !
Safran|3|False|?!
Écriture|4|True|14
Écriture|5|False|849
Pepper|6|False|Tap
Lord Azeirf|7|True|Tap
Lord Azeirf|8|True|Tap
Lord Azeirf|9|False|Tap
Écriture|10|True|18
Écriture|11|False|231
Maire de Komona|12|True|S'il vous plaît, s'il vous plaît ! Un peu de tenue !
Maire de Komona|13|False|Shichimi et Coriandre sont éliminées !
Maire de Komona|14|False|Camomille, Spiruline et Safran sont donc qualifiées pour la finale !
<hidden>|0|False|Edit this one, all others are linked
Audience|2|False|Clap

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Maire de Komona|1|False|Mesdemoiselles, êtes-vous prêtes pour cette dernière épreuve ?
Maire de Komona|2|False|À vous de jouer !
Écriture|3|True|1 5|nowhitespace
Écriture|4|False|703
Écriture|5|True|19
Écriture|6|False|863
Écriture|7|True|1 3|nowhitespace
Écriture|8|False|614
Son|10|False|Poum !
Pepper|11|False|Je retire tout ce que j'ai pu dire sur ce système…
Lord Azeirf|12|True|Tap
Lord Azeirf|13|True|Tap
Lord Azeirf|14|False|Tap
Narrateur|15|False|- FIN -
<hidden>|0|False|Edit this one, all others are linked
Audience|9|False|Clap

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crédits|1|False|05/2017 - www.peppercarrot.com - Dessin & Scénario : David Revoy
Crédits|2|False|Amélioration des dialogues : Nicolas Artance, Valvin
Crédits|3|False|Basé sur l'univers d'Hereva créé par David Revoy avec les contributions de Craig Maloney. Corrections de Willem Sonke, Moini, Hali, CGand et Alex Gryson.
Crédits|4|False|Logiciels : Krita 3.1.3, Inkscape 0.92.1 sur Linux Mint 18.1
Crédits|5|False|Licence : Creative Commons Attribution 4.0
Crédits|6|False|Pepper&Carrot est entièrement libre, open-source, et sponsorisé grâce au mécénat de ses lecteurs. Pour cet épisode, merci aux 864 Mécènes :
Crédits|7|False|Vous aussi, devenez mécène de Pepper&Carrot sur www.patreon.com/davidrevoy
