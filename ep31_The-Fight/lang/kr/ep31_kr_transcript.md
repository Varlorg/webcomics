# Transcript of Pepper&Carrot Episode 31 [kr]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
제목|1|False|제31 화: 결투

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
소리|4|False|부우우우 우우웅|nowhitespace
소리|5|False|피슈우우웅!
소리|8|False|쉬이이익!
후추|10|False|! !|nowhitespace
해설|1|False|테네부룸, 카오사의 성스러운 봉우리.
소리|2|False|부우우우우웅 !
후추|3|False|쯧!
후추|6|False|받아라!
카옌|9|False|어이 초짜!
소리|11|False|쾅!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
소리|6|False|후우우우우욱! !
소리|7|False|치쉬쉬쉬쉬쉭! !|nowhitespace
소리|8|False|치쉬쉬쉬쉬쉭! !|nowhitespace
소리|1|False|푸슈우우우우웅!
소리|2|False|쩌저 저적! !!|nowhitespace
소리|3|False|쩌 저 저 적 ! !!|nowhitespace
후추|4|False|그냥은 못 져!
후추|5|False|그라위타티오나스 스히엘두스!
소리|9|False|툭!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
소리|5|False|푸슈우우우우 !|nowhitespace
소리|3|False|슈 우 우우 웅|nowhitespace
카옌|6|False|?!!
카옌|12|False|! !
카옌|13|False|크으윽...
글자|14|False|2019-12-20-E31P03_V15-최종본.jpg
글자|15|False|이미지 로딩 실패
후추|1|True|당근!
후추|2|False|플랜 7-B!
후추|4|False|화질화질 나빠지스!
카옌|10|False|으아악!!
소리|7|True|지
소리|8|True|지|nowhitespace
소리|9|False|직|nowhitespace
후추|11|False|화질화질 구리구리스!
소리|16|False|와장창 ! !!|nowhitespace

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
카옌|1|False|퓌로 봄바 아토미쿠스!
소리|2|False|피슈우우우우웅!
소리|3|True|퍼
소리|4|True|어|nowhitespace
소리|5|True|엉|nowhitespace
소리|6|True|!|nowhitespace
소리|7|False|!|nowhitespace
소리|8|True|쿠구구구
소리|10|False|푸 쉬 이이 이 ...
소리|9|False|쿠구구구

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
카옌|1|False|풋...
카옌|2|True|그 나이에 미공 으로 삼십육계 줄행랑을 치시겠다?
카옌|3|False|참말로 한심한 패배로구만...
후추|4|True|땡이요!
후추|5|False|웜홀이었습니다!
소리|6|False|파직! !
후추|7|False|출구 내줘서 고마워, 당근!
후추|8|False|체크메이트입니다, 카옌 스승님!
후추|9|False|구르게스...
후추|10|False|...아테르!
소리|12|False|쉬이이이이이이이우우우우우우우우우욱!!!
소리|11|False|부 우 우 우웅 ! !!
백리향|13|False|그만!!!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
백리향|1|True|그만 하랬다!
백리향|2|False|이제 됐어!
소리|3|False|딱!
소리|4|False|피 유웅 !
소리|5|False|쓰윽!
백리향|6|True|너희 둘!
백리향|7|True|즉시!
백리향|8|False|여기로 온다!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
백리향|5|False|그래서...?
카옌|7|True|그래...
글자|15|False|~ 후추에게 수여함 ~
소리|1|False|툭!
소리|2|False|툭!
백리향|3|True|"마지막으로 한 번만 점검"한다고 한지 벌써 3년이 지났어...
백리향|4|False|...우린 여기서 날밤을 다 세우려고 온 게 아니야!
후추|6|False|...
카옌|8|True|...학위를 주도록 하지...
카옌|9|False|...하지만 "턱걸이"인 걸로.
글자|10|True|카오사
글자|11|False|학위증
글자|13|False|커민
글자|12|False|카옌
글자|14|False|백리향
글자|16|False|공식 마녀
해설|17|False|- 끝 -

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
크레딧|1|False|2019년 12월 20일 그림 및 이야기 : David Revoy. 베타 독자 : Craig Maloney, Martin Disch, Arlo James Barnes Nicolas Artance, Valvin. 한글판 번역 : Jihoon Kim. 헤레바 세계관을 기반으로 함 원작자 : David Revoy. 수석 감독 : Craig Maloney. 작가 : Craig Maloney, Nartance, Scribblemaniac, Valvin. 수정 : Willem Sonke, Moini, Hali, CGand, Alex Gryson . 소프트웨어 : 쿠분투 18.04-LTS에서 크리타 4.2.6appimage, 잉크스케이프 0.92.3 사용. 라이선스 : 크리에이티브 커먼즈 저작자표시 4.0. www.peppercarrot.com
후추|3|True|후추와 당근은 완전히 자유 오픈소스이고 독자들의 후원을 받아 운영하고 있습니다.
후추|4|False|이번 화에 도움을 주신 971명의 후원자분께 감사드립니다!
후추|5|True|여러분도 후추와 당근의 후원자가 되어서 여기에 이름을 올려보세요!
후추|6|True|후원은 Patreon, Tipeee, Paypal, Liberapay ...등에서 할 수 있어요!
후추|7|True|자세한 내용은 www.peppercarrot.com을 참고하세요!
후추|8|False|감사합니다!
후추|2|True|그거 아세요?
<hidden>|0|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|0|False|You can also translate this page if you want.
<hidden>|0|False|Beta readers help with the story, proofreaders give feedback about the text.
