# Transcript of Pepper&Carrot Episode 31 [gd]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tiotal|1|False|Eapasod 31: A’ chòmhrag

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Neach-aithris|1|False|Aig Sgùrr Sgùraidh, cnoc coisrigte na Dubh-choimeasgachd.
Fuaim|2|False|FRuuu m !|nowhitespace
Peabar|3|False|Tud!
Fuaim|4|False|FRuuu um|nowhitespace
Fuaim|5|False|Brsiuuu!
Peabar|6|False|Gabh seo!
Fuaim|8|False|Isss!
Cìob|9|False|Ùranaich!
Peabar|10|False|! !|nowhitespace
Fuaim|11|False|SPLAD !|nowhitespace

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fuaim|1|False|Drssuubh!
Fuaim|2|False|BR AG! !!|nowhitespace
Fuaim|3|False|BR R A G ! !!|nowhitespace
Peabar|4|False|Fuirich ort!
Peabar|5|False|SGIATHUS GRAVITATIONAS!
Fuaim|6|False|Fruuuiss! !|nowhitespace
Fuaim|7|False|Tigisgg !|nowhitespace
Fuaim|8|False|Tigisgg! !|nowhitespace
Fuaim|9|False|Poc!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|1|True|A CHURRAIN!
Peabar|2|False|Plana 7-B!
Peabar|4|False|CÀILEACHDUS JPEGIS!
Fuaim|5|False|Brs iuuu !|nowhitespace
Fuaim|3|False|Fr u is|nowhitespace
Cìob|6|False|?!!
Cìob|10|False|Mo chreach!!
Fuaim|7|True|G
Fuaim|8|True|S|nowhitespace
Fuaim|9|False|S|nowhitespace
Peabar|11|False|CÀILEACHDUS LUGHATHUS!
Cìob|12|False|! !|nowhitespace
Cìob|13|False|Grr...
Sgrìobhte|14|False|2019-12-20-E31P03_T15-deireannach.jpg
Sgrìobhte|15|False|Mearachd 'ga luchdadh
Fuaim|16|False|TUIS ! !!|nowhitespace

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cìob|1|False|BOMA TEINE ATAMACHUS!
Fuaim|2|False|Frrsaabh!
Fuaim|3|True|B
Fuaim|4|True|A|nowhitespace
Fuaim|5|True|B|nowhitespace
Fuaim|6|True|U|nowhitespace
Fuaim|7|True|U|nowhitespace
Fuaim|8|True|M|nowhitespace
Fuaim|9|True|!|nowhitespace
Fuaim|10|False|!|nowhitespace
Fuaim|11|True|BRRR
Fuaim|12|False|BRRR
Fuaim|13|False|P s ss i ...|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cìob|1|False|Tiud...
Cìob|2|True|A’ teicheadh gu meanbh-chruinne air d’ aois-sa?
Cìob|3|False|Abair ruaig air gealltair ...
Peabar|4|True|Ceàrr!
Peabar|5|False|Seo toll-cnuimhe!
Fuaim|6|False|Bss! !|nowhitespace
Peabar|7|False|Mòran taing airson na slighe a-mach, a Churrain!
Peabar|8|False|Tul-chasg, a’ mhaighstir Chìob!!
Peabar|9|False|GURGES...
Peabar|10|False|...ATER!
Fuaim|12|False|Sbhhhhhiiiiiiiibbbb!!!
Fuaim|11|False|F R U UM ! !!|nowhitespace
Tìom|13|False|STADAIBH!!!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tìom|1|True|Thuirt mi gun STAD sibh!
Tìom|2|False|Seo tuilleadh ’s a chòir!
Fuaim|3|False|Cnag !|nowhitespace
Fuaim|4|False|BR RAG !|nowhitespace
Fuaim|5|False|TIUD!
Tìom|6|True|An dithis agaibhse!
Tìom|7|True|AN-SEO!
Tìom|8|False|Gun dàil!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fuaim|1|False|Beum!
Fuaim|2|False|Beum!
Tìom|3|True|’S ann o chionn trì bliadhna a thuirt thu rinn “dìreach aon deuchainn beag bìodach eile”…
Tìom|4|False|…cha chuir sinn seachad an oidhche air fad an-seo!
Tìom|5|False|Nise…?
Peabar|6|False|...
Cìob|8|True|…gheibh i a ceumnachadh…
Cìob|9|False|…ach le comharra “cuibheasach”.
Sgrìobhte|10|True|An Dubh-
Sgrìobhte|11|False|choimeasgachd|nowhitespace
Sgrìobhte|15|False|Carabhaidh
Sgrìobhte|13|False|Cìob
Sgrìobhte|14|False|T ì om|nowhitespace
Sgrìobhte|17|False|~ Peabar ~
Sgrìobhte|16|False|bana-bhuidseach oifigeil
Neach-aithris|18|False|- Deireadh na sgeòil -
Cìob|7|True|Ceart ma-thà…
Sgrìobhte|12|False|~ CEUM ~

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|5|True|’S urrainn dhut dol ’nad phàtran Pepper&Carrot cuideachd is chì thu d’ àinm an-seo!
Peabar|3|True|Tha Pepper&Carrot saor is le tùs fosgailte air fad agus ’ga sponsaireadh le taic nan leughadairean.
Peabar|4|False|Mòran taing dhan 971 pàtran a thug taic dhan eapasod seo!
Peabar|7|True|Tadhail air www.peppercarrot.com airson barrachd fiosrachaidh!
Peabar|6|True|Tha sinn air Patreon, Tipeee, PayPal, Liberapay ...’s a bharrachd!
Peabar|8|False|Mòran taing!
Peabar|2|True|An robh fios agad?
Urram|1|False|20 dhen Dùbhlachd 2019 Obair-ealain ⁊ sgeulachd: David Revoy. Leughadairean Beta: Craig Maloney, Martin Disch, Arlo James Barnes, Nicolas Artance, Valvin. Tionndadh Gàidhlig Eadar-theangachadh: GunChleoc . Stèidhichte air saoghal Hereva Air a chruthachadh le: David Revoy. Prìomh neach-glèidhidh: Craig Maloney. Sgrìobhadairean: Craig Maloney, Nartance, Scribblemaniac, Valvin. Ceartachadh: Willem Sonke, Moini, Hali, CGand, Alex Gryson . Bathar-bog: Krita 4.2.6appimage, Inkscape 0.92.3 air Kubuntu 18.04-LTS. Ceadachas: Creative Commons Attribution 4.0. www.peppercarrot.com
<hidden>|0|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|0|False|You can also translate this page if you want.
<hidden>|0|False|Beta readers help with the story, proofreaders give feedback about the text.
