# Transcript of Pepper&Carrot Episode 31 [id]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Judul|1|False|Episode 31: Pertarungan

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narator|1|False|Tenebrume, bukit keramat Chaosah.
Suara|2|False|BRmmm!|nowhitespace
Pepper|3|False|Tsk!
Suara|4|False|BRmmm|nowhitespace
Suara|5|False|Brzuu!
Pepper|6|False|Rasakan itu!
Suara|8|False|Schh!
Cayenne|9|False|Amatir!
Pepper|10|False|! !|nowhitespace
Suara|11|False|BLAM !|nowhitespace

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Suara|1|False|Drzzuuw!
Suara|2|False|KREKK! !!|nowhitespace
Suara|3|False|KREEK K ! !!|nowhitespace
Pepper|4|False|Tak secepat itu!
Pepper|5|False|GRAVITATIONAS SHIELDUS!
Suara|6|False|Whuush! !|nowhitespace
Suara|7|False|Tchkshkk! !|nowhitespace
Suara|8|False|Tchkshkk! !|nowhitespace
Suara|9|False|Tok!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|CARROT!
Pepper|2|False|Rencana 7-B!
Pepper|4|False|JPEGUS QUALITIS!
Suara|5|False|Brzuuu !|nowhitespace
Suara|3|False|Zuu umm|nowhitespace
Cayenne|6|False|?!!
Cayenne|10|False|Argh!!
Suara|7|True|G
Suara|8|True|Z|nowhitespace
Suara|9|False|Z|nowhitespace
Pepper|11|False|QUALITIS MINIMALIS!
Cayenne|12|False|! !|nowhitespace
Cayenne|13|False|Grr...
Penulis|14|False|2019-12-20-E31P03_V15-final.jpg
Penulis|15|False|Gagal Memuat
Suara|16|False|PRANG ! !!|nowhitespace

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|1|False|PYRO BOMBA ATOMICUS!
Suara|2|False|Frrzuuuw!
Suara|3|True|D
Suara|4|True|U|nowhitespace
Suara|5|True|A|nowhitespace
Suara|6|True|A|nowhitespace
Suara|7|True|R|nowhitespace
Suara|8|True|R|nowhitespace
Suara|9|True|!|nowhitespace
Suara|10|False|!|nowhitespace
Suara|11|True|BRRR
Suara|12|False|BRRR
Suara|13|False|P s hh h ...|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|1|False|Pfft...
Cayenne|2|True|Seumur kamu kaburnya ke dimensi-mikro ?
Cayenne|3|False|Kekalahan yang menyedihkan ...
Pepper|4|True|Salah!
Pepper|5|False|Wormhole!
Suara|6|False|Bzz! !|nowhitespace
Pepper|7|False|Terima kasih untuk lubang keluarnya, Carrot!
Pepper|8|False|Skakmat, master Cayenne!
Pepper|9|False|GURGES...
Pepper|10|False|...ATER!
Suara|12|False|Swwwwwwiiiiiiiipppp!!!
Suara|11|False|B RUM M ! !!|nowhitespace
Thyme|13|False|BERHENTI!!!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Thyme|1|True|Aku bilang BERHENTI!
Thyme|2|False|Sudah cukup!
Suara|3|False|Ptik !|nowhitespace
Suara|4|False|PU FF !|nowhitespace
Suara|5|False|TCHK!
Thyme|6|True|Kalian berdua!
Thyme|7|True|KE SINI!
Thyme|8|False|Sekarang!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Suara|1|False|Tok!
Suara|2|False|Tok!
Thyme|3|True|Sudah tiga tahun sejak kamu bilang "hanya ujian satu lagi"...
Thyme|4|False|...kita tidak akan menghabiskan semalaman di sini!
Thyme|5|False|Jadi...?
Pepper|6|False|...
Cayenne|7|True|Baik...
Cayenne|8|True|...dia boleh dapat ijazahnya...
Cayenne|9|False|...tapi dengan nilai "pas-pasan".
Penulis|10|True|Ijazah
Penulis|12|False|Chaosah
Penulis|13|False|Cumin
Penulis|14|False|Cayenne
Penulis|15|False|T h yme|nowhitespace
Penulis|16|False|~ Pepper ~
Penulis|17|False|Penyihir Resmi
Penulis|18|False|- TAMAT -
Penulis|11|False|Kelulusan

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|5|True|Anda juga bisa menjadi sukarelawan Pepper&Carrot dan dapatkan namamu di sini!
Pepper|3|True|Pepper&Carrot sepenuhnya gratis (bebas), sumber terbuka dan disponsori oleh para pembacanya.
Pepper|4|False|Untuk episode ini, terima kasih untuk 971 sukarelawan!
Pepper|7|True|Cek www.peppercarrot.com untuk informasi selanjutnya!
Pepper|6|True|Kami ada di Patreon, Tipeee, PayPal, Liberapay ...and banyak lagi!
Pepper|8|False|Terima kasih!
Pepper|2|True|Tahukah kamu?
Kredit|1|False|20 Desember 2019 Pelukis & Skenario: David Revoy. Penguji bacaan cerita: Craig Maloney, Martin Disch, Arlo James Barnes, Nicolas Artance, Valvin. Versi Bahasa Indonesia Terjemahan: Aldrian Obaja Muis . Berdasarkan alam semesta Hereva Pembuat: David Revoy. Penyunting utama: Craig Maloney. Penulis: Craig Maloney, Nartance, Scribblemaniac, Valvin. Pemeriksa: Willem Sonke, Moini, Hali, CGand, Alex Gryson . Peranti lunak: Krita 4.2.6appimage, Inkscape 0.92.3 on Kubuntu 18.04-LTS. Izin (lisensi): Creative Commons Attribution 4.0. www.peppercarrot.com
<hidden>|0|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|0|False|You can also translate this page if you want.
<hidden>|0|False|Beta readers help with the story, proofreaders give feedback about the text.
