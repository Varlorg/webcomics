# Transcript of Pepper&Carrot Episode 25 [da]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 25: Ingen genveje

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Skrift|1|False|KATTEMUMS

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fortæller|13|False|- SLUT -
Pepper|1|True|?
Pepper|2|True|?
Pepper|3|True|?
Pepper|4|False|?
Pepper|5|True|?
Pepper|6|True|?
Pepper|7|True|?
Pepper|8|False|?
Pepper|9|True|?
Pepper|10|True|?
Pepper|11|True|?
Pepper|12|False|?

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|7|False|Støt næste episode af Pepper&Carrot på www.patreon.com/davidrevoy
Credits|6|False|Pepper&Carrot er helt gratis, open-source, og sponsoreret af læsere. Til denne episode, tak til de 909 tilhængere:
Credits|1|False|05/2018 - www.peppercarrot.com - Tegning og manuskript: David Revoy - Dansk oversættelse: Alexandre Alapetite
Credits|3|False|Baseret på Hereva-universet skabt af David Revoy med bidrag af Craig Maloney. Rettelser af Willem Sonke, Moini, Hali, CGand og Alex Gryson.
Credits|4|False|Værktøj: Krita 4.0.0, Inkscape 0.92.3 på Kubuntu 17.10
Credits|5|False|Licens: Creative Commons Attribution 4.0
Credits|2|False|Feedback på beta-versionen: Nicolas Artance, Imsesaok, Craig Maloney, Midgard, Valvin, xHire et Zveryok.
